package com.jaewa.jakebovoice.controller;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;

import com.jaewa.jakebovoice.speechParser.ContentType;
import com.jaewa.jakebovoice.speechParser.DateUtils;
import com.jaewa.jakebovoice.speechParser.ParserType;
import com.jaewa.jakebovoice.speechParser.Speech;

/**
 * Created by cristian on 16/10/17.
 */

public class SearchController extends VocalCommandsController {

    public SearchController(Activity activity, Context context){
        super(activity, context);
    }

    public Cursor findMovements(String period, String fromDate, String toDate, String category) {

        Cursor result;

        try{
            if( "Sempre".equals(period) && "Tutte".equals(category) ){
                result = dbManager.findAllMovements();
            }else if ( "Sempre".equals(period) && !"Tutte".equals(category) ){
                result = dbManager.findMovementsByCategory(category );
            }else if ( !"Sempre".equals(period) && "Tutte".equals(category) ){
                result = dbManager.findMovementsByPeriod( DateUtils.formatDateForDB(fromDate), DateUtils.formatDateForDB(toDate) );
            }else{
                result = dbManager.findMovementsByPeriodAndCategory( DateUtils.formatDateForDB(fromDate), DateUtils.formatDateForDB(toDate), category );
            }

        }catch (Exception e){
            System.out.println(e);
            return null;
        }
        return result;
    }

    public Speech processVocalSearch(String speechText){

        Speech speech = new Speech();
        speech.parse( speechText );

        if( speech.get(ContentType.OPERATION)=="" || !ParserType.SEARCH.toString().equals(speech.get(ContentType.OPERATION)) ){
            return null;
        }else{
            return speech;
        }

    }
}
