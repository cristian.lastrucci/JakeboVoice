package com.jaewa.jakebovoice.controller;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.widget.Toast;

import com.jaewa.jakebovoice.R;

import java.util.Locale;

/**
 * Created by cristian on 15/10/17.
 */

public abstract class VocalCommandsController extends BaseController {

    VocalCommandsController(Activity activity, Context context){
        super(activity, context);
    }

    private final int REQ_CODE_SPEECH_INPUT = 100;

    /**
     * Show google speech input dialog
     * */
    public void startVocalCommandListening() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, context.getString(R.string.speech_prompt));

        try {
            activity.startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(context, context.getString(R.string.speech_not_supported), Toast.LENGTH_SHORT).show();
        }
    }
}
