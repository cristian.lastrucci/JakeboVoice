package com.jaewa.jakebovoice.controller;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.Spanned;

import com.jaewa.jakebovoice.R;
import com.jaewa.jakebovoice.dataStorage.DbManager;
import com.jaewa.jakebovoice.speechParser.ContentType;
import com.jaewa.jakebovoice.speechParser.Speech;
import com.jaewa.jakebovoice.view.InsertionActivity;
import com.jaewa.jakebovoice.view.MainActivity;
import com.jaewa.jakebovoice.view.SearchActivity;
import com.jaewa.jakebovoice.view.SearchParamsActivity;
import com.jaewa.jakebovoice.view.VocalGuideActivity;

/**
 * Created by cristian on 15/10/17.
 */

public abstract class BaseController {

    DbManager dbManager;

    Activity activity;
    Context context;

    BaseController(Activity activity, Context context){
        this.activity = activity;
        this.context = context;
        dbManager = new DbManager( context );
    }

    public void openDBConnection(){
        dbManager.openConnection();
    }

    public void closeDBConnection(){
        dbManager.closeConnection();
    }

    public void startMainActivity(){
        Intent intent = new Intent(context, MainActivity.class);
        activity.startActivity(intent);
    }

    public void startInsertionActivity(){
        Intent intent = new Intent(context, InsertionActivity.class);
        activity.startActivity(intent);
    }

    public void startInsertionActivity(Speech speech) {
        Intent intent = new Intent(context, InsertionActivity.class);
        intent.putExtra( "speech", speech.getSpeechText() );
        intent.putExtra( "date", speech.get(ContentType.DATE) );
        intent.putExtra( "time", speech.get(ContentType.TIME) );
        intent.putExtra( "amount", speech.get(ContentType.AMOUNT) );
        intent.putExtra( "category", speech.get(ContentType.CATEGORY) );
        intent.putExtra( "description", speech.get(ContentType.DESCRIPTION) );
        activity.startActivity(intent);
    }

    public void startSearchActivity(){
        Intent intent = new Intent(context, SearchActivity.class);
        activity.startActivity(intent);
    }

    public void startSearchActivity(Speech speech) {
        Intent intent = new Intent(context, SearchActivity.class);
        intent.putExtra( "periodFrom", speech.get(ContentType.DATE_FROM) );
        intent.putExtra( "periodTo", speech.get(ContentType.DATE_TO) );
        intent.putExtra( "category", speech.get(ContentType.CATEGORY) );
        activity.startActivity(intent);
    }

    public void startSearchParamsActivity(String fromDate, String toDate, String category) {
        Intent intent = new Intent(context, SearchParamsActivity.class);
        intent.putExtra( "periodFrom", fromDate );
        intent.putExtra( "periodTo", toDate );
        intent.putExtra( "category", category );
        activity.startActivity(intent);
    }

    public void startVocalGuideActivity(){
        Intent intent = new Intent(context, VocalGuideActivity.class);
        activity.startActivity(intent);
    }

    public AlertDialog createVocalOperationErrorDialog(String title, String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        AlertDialog dialog;

        builder.setIcon(R.drawable.danger_orange)
                .setTitle(title)
                .setMessage(fromHtml(message));

        builder.setPositiveButton("Vai alla Guida", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(context, VocalGuideActivity.class);
                activity.startActivity(intent);
            }
        });

        builder.setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        dialog = builder.create();
        return dialog;
    }

    public AlertDialog createInfoDialog(String title, String message, int idIcon) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        AlertDialog dialog;

        builder.setIcon(idIcon)
                .setTitle(title)
                .setMessage(fromHtml(message));

        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        dialog = builder.create();
        return dialog;
    }

    @SuppressWarnings("deprecation")
    private static Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
}
