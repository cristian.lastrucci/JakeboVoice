package com.jaewa.jakebovoice.controller;

import android.app.Activity;
import android.content.Context;

import com.jaewa.jakebovoice.speechParser.AmountParser;
import com.jaewa.jakebovoice.speechParser.ContentType;
import com.jaewa.jakebovoice.speechParser.DateUtils;
import com.jaewa.jakebovoice.speechParser.ParserType;
import com.jaewa.jakebovoice.speechParser.Speech;

/**
 * Created by cristian on 15/10/17.
 */

public class InsertionController extends VocalCommandsController {

    public InsertionController(Activity activity, Context context){
        super(activity, context);
    }

    public Speech processVocalInsertion(String speechText){

        Speech speech = new Speech();
        speech.parse( speechText );

        if( speech.get(ContentType.OPERATION)=="" || !ParserType.INSERTION.toString().equals(speech.get(ContentType.OPERATION)) ){
            return null;
        }else{
            return speech;
        }

    }

    public boolean saveMovement(String date, String time, String amount, String category, String description) {
        try {
            dbManager.openConnection();
            dbManager.insertMovement(DateUtils.formatDatetimeForDB(date, time),
                                    AmountParser.formatAmount( Float.parseFloat(amount) ),
                                    notEmptyString( category ),
                                    notEmptyString(description));
            dbManager.closeConnection();
            return true;
        }catch (Exception e){
            return false;
        }
    }

    private String notEmptyString(String s){
        if( s == null || "".equals(s) ){
            return null;
        }
        return s;
    }
}
