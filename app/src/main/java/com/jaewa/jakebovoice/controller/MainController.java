package com.jaewa.jakebovoice.controller;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.support.v7.app.AlertDialog;

import com.jaewa.jakebovoice.dataStorage.DbInitializer;
import com.jaewa.jakebovoice.dataStorage.DbUtils;
import com.jaewa.jakebovoice.speechParser.ContentType;
import com.jaewa.jakebovoice.speechParser.DateUtils;
import com.jaewa.jakebovoice.speechParser.ParserType;
import com.jaewa.jakebovoice.speechParser.Speech;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cristian on 15/10/17.
 */

public class MainController extends VocalCommandsController {

    public MainController(Activity activity, Context context){
        super(activity, context);
        initEmptyDB();
    }

    private void initEmptyDB() {

        try{
            long movementsCount;

            dbManager.openConnection();
            movementsCount = dbManager.getMovementsCount();
            dbManager.closeConnection();

            if (movementsCount == 0){
                DbInitializer.initDb(dbManager);
            }

        }catch (Exception e){
            System.out.println(e);
        }
    }

    public String getCurrentMonth() {
        Calendar date = Calendar.getInstance();
        String year = String.valueOf( date.get(Calendar.YEAR) );
        String month = DateUtils.parseMonth( date.get(Calendar.MONTH) );
        return month + " " + year;
    }

    public void processVoiceCommand(String speechText){
        Speech speech = new Speech();
        speech.parse( speechText );

        if( "".equals(speech.get(ContentType.OPERATION)) ){
            AlertDialog dialog = createVocalOperationErrorDialog("Comando vocale non riconosciuto", "Vuoi leggere la guida dei comandi vocali per imparare come utilizzarli?");
            dialog.show();
        }else if( ParserType.INSERTION.toString().equals(speech.get(ContentType.OPERATION)) ){
            startInsertionActivity(speech);
        }else if( ParserType.SEARCH.toString().equals(speech.get(ContentType.OPERATION)) ){
            startSearchActivity(speech);
        }
    }

    public Map<String, Float> calculateMonthIndicators(){

        Map<String, Float> values = new HashMap<>();

        Calendar firstOfMonth = Calendar.getInstance();
        firstOfMonth.set(Calendar.DAY_OF_MONTH, 1);

        Calendar lastOfMonth = Calendar.getInstance();
        lastOfMonth.set(Calendar.DAY_OF_MONTH, lastOfMonth.getActualMaximum(Calendar.DAY_OF_MONTH));

        try{

            dbManager.openConnection();
            values.put("monthIncome", calculateMonthIncome(firstOfMonth, lastOfMonth) );
            values.put("monthOutflow", calculateMonthOutflow(firstOfMonth, lastOfMonth));
            values.put("savings", calculateSavings());
            values.put("available", calculateAvailable(values));
            dbManager.closeConnection();

        }catch (SQLException e){
            System.out.println(e);
            values = new HashMap<>();
            values.put("monthIncome", 0f );
            values.put("monthOutflow", 0f);
            values.put("savings", 0f);
            values.put("available", 0f);
        }

        return values;
    }

    private Float calculateMonthIncome(Calendar firstOfMonth, Calendar lastOfMonth){
        Cursor cursor = dbManager.findIncome(DateUtils.formatDateForDB(firstOfMonth), DateUtils.formatDateForDB(lastOfMonth));
        float monthIncome = 0;
        while (cursor.moveToNext()){
            String amount = cursor.getString( cursor.getColumnIndex(DbUtils.FIELD_AMOUNT) );
            monthIncome = monthIncome + Float.parseFloat(amount);
        }
        return monthIncome;
    }

    private Float calculateMonthOutflow(Calendar firstOfMonth, Calendar lastOfMonth) {
        Cursor cursor = dbManager.findOutflow(DateUtils.formatDateForDB(firstOfMonth), DateUtils.formatDateForDB(lastOfMonth));
        float monthOutflow = 0;
        while (cursor.moveToNext()){
            String amount = cursor.getString( cursor.getColumnIndex(DbUtils.FIELD_AMOUNT) );
            monthOutflow = monthOutflow + Float.parseFloat(amount);
        }
        return monthOutflow;
    }

    private Float calculateSavings(){
        Calendar firstDay = Calendar.getInstance();
        firstDay.set(Calendar.YEAR, 1000);
        firstDay.set(Calendar.MONTH, 0);
        firstDay.set(Calendar.DAY_OF_MONTH, 1);

        Calendar lastDayOfPreviousMonth = Calendar.getInstance();
        lastDayOfPreviousMonth.set(Calendar.DAY_OF_MONTH, 1);
        lastDayOfPreviousMonth.add(Calendar.DAY_OF_YEAR, -1);

        Cursor cursor = dbManager.findIncome(DateUtils.formatDateForDB(firstDay), DateUtils.formatDateForDB(lastDayOfPreviousMonth));
        float totalIncomeBeforeThisMonth = 0;
        while (cursor.moveToNext()){
            String amount = cursor.getString( cursor.getColumnIndex(DbUtils.FIELD_AMOUNT) );
            totalIncomeBeforeThisMonth = totalIncomeBeforeThisMonth + Float.parseFloat(amount);
        }

        cursor = dbManager.findOutflow(DateUtils.formatDateForDB(firstDay), DateUtils.formatDateForDB(lastDayOfPreviousMonth));
        float totalOutflowBeforeThisMonth = 0;
        while (cursor.moveToNext()){
            String amount = cursor.getString( cursor.getColumnIndex(DbUtils.FIELD_AMOUNT) );
            totalOutflowBeforeThisMonth = totalOutflowBeforeThisMonth + Float.parseFloat(amount);
        }

        return totalIncomeBeforeThisMonth - totalOutflowBeforeThisMonth;
    }

    private Float calculateAvailable(Map<String, Float> values) {
        return values.get("savings") + values.get("monthIncome") - values.get("monthOutflow");
    }
}