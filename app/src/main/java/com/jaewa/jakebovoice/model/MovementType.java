package com.jaewa.jakebovoice.model;

/**
 * Created by cristian on 21/09/17.
 */

public enum MovementType {

    INCOME, SUBSISTENCE, OPTIONAL, HOBBY, ACCIDENT;

    public static String itString(MovementType mt)
    {
        if (MovementType.INCOME.equals(mt)){
            return "Entrata";
        }
        if (MovementType.SUBSISTENCE.equals(mt)){
            return "Sussistenza";
        }
        if (MovementType.OPTIONAL.equals(mt)){
            return "Optional";
        }
        if (MovementType.HOBBY.equals(mt)){
            return "Hobby";
        }
        if (MovementType.ACCIDENT.equals(mt)){
            return "Inaspettata";
        }
        return "";
    }
}