package com.jaewa.jakebovoice.view;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.jaewa.jakebovoice.R;
import com.jaewa.jakebovoice.controller.InsertionController;
import com.jaewa.jakebovoice.model.MovementType;
import com.jaewa.jakebovoice.speechParser.ContentType;
import com.jaewa.jakebovoice.speechParser.Speech;

import java.util.ArrayList;
import java.util.Calendar;

import static com.jaewa.jakebovoice.R.layout.insertion_activity;

/**
 * Created by cristian on 26/09/17.
 */

public class InsertionActivity extends AppCompatActivity {

    private InsertionController controller;
    private final int REQ_CODE_SPEECH_INPUT = 100;

    private EditText datePkr;
    private EditText timePkr;
    private EditText amountTB;
    private Spinner categoryCB;
    private EditText descriptionTB;
    private ImageButton speakBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(insertion_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        controller = new InsertionController(this, getApplicationContext());

        loadData();
    }

    private void loadData() {

        Intent intent = getIntent();

        loadDate( intent.getStringExtra("date") );
        loadTime( intent.getStringExtra("time") );
        loadAmount(intent.getStringExtra("amount"));
        loadCategory( intent.getStringExtra("category") );
        loadDescription(intent.getStringExtra("description"));

        initVocalCommandsBtn();
    }

    private void loadDate( String date ) {

        datePkr = (EditText) findViewById(R.id.datePkr);

        if(date != null && !"".equals(date) ){
            datePkr.setText(date);
        }else{
            Calendar calendar = Calendar.getInstance();
            String year = String.valueOf(calendar.get(Calendar.YEAR));
            String month = calendar.get(Calendar.MONTH)<9 ? "0" + String.valueOf(calendar.get(Calendar.MONTH)+1) : String.valueOf(calendar.get(Calendar.MONTH)+1);
            String day = calendar.get(Calendar.DAY_OF_MONTH)<10 ? "0" + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)) : String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            datePkr.setText(day + "/" + month + "/" + year);
        }

        datePkr.setOnFocusChangeListener( new View.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    String[] currentDate = datePkr.getText().toString().split("/");
                    Calendar calendar = Calendar.getInstance();
                    Integer year = currentDate.length == 3 ? Integer.parseInt(currentDate[2]) : calendar.get(Calendar.YEAR);
                    Integer month = currentDate.length == 3 ? Integer.parseInt(currentDate[1])-1 : calendar.get(Calendar.MONTH);
                    Integer day = currentDate.length == 3 ? Integer.parseInt(currentDate[0]) : calendar.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog dpDialog = new DatePickerDialog(InsertionActivity.this, new DatePickerDialog.OnDateSetListener(){
                        @Override
                        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                            String month = selectedMonth<9 ? "0" + String.valueOf(selectedMonth+1) : String.valueOf(selectedMonth+1);
                            String day = selectedDay<10 ? "0" + String.valueOf(selectedDay) : String.valueOf(selectedDay);
                            datePkr.setText(day + "/" + month + "/" + String.valueOf(selectedYear));
                        }
                    }, year, month, day );
                    dpDialog.show();
                    datePkr.clearFocus();
                }
            }
        });
    }

    private void loadTime(String time) {
        timePkr = (EditText) findViewById(R.id.timePkr);

        if( time != null && !"".equals(time) ){
            timePkr.setText(time);
        }else{
            Calendar calendar = Calendar.getInstance();
            String hour = calendar.get(Calendar.HOUR_OF_DAY)<10 ? "0" + String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)) : String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
            String minute = calendar.get(Calendar.MINUTE)<10 ? "0" + String.valueOf(calendar.get(Calendar.MINUTE)) : String.valueOf(calendar.get(Calendar.MINUTE));
            timePkr.setText(hour + ":" + minute);
        }

        timePkr.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    String[] currentTime = timePkr.getText().toString().split(":");
                    Calendar calendar = Calendar.getInstance();
                    Integer hour = currentTime.length==2 ? Integer.parseInt(currentTime[0]) : calendar.get(Calendar.HOUR_OF_DAY);
                    Integer minute = currentTime.length==2 ? Integer.parseInt(currentTime[1]) : calendar.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker = new TimePickerDialog(InsertionActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String hour = selectedHour<10 ? "0"+String.valueOf(selectedHour) : String.valueOf(selectedHour);
                            String minute = selectedMinute<10 ? "0"+String.valueOf(selectedMinute) : String.valueOf(selectedMinute);
                            timePkr.setText(hour + ":" + minute);
                        }
                    }, hour, minute, true);
                    mTimePicker.show();
                    timePkr.clearFocus();
                }
            }
        });
    }

    private void loadAmount(String amount){
        amountTB = (EditText) findViewById(R.id.amountTB);
        amountTB.setText(amount);
    }

    private void loadCategory( String selectedCategory) {
        categoryCB = (Spinner) findViewById(R.id.categoryCB);
        MovementType[] types = MovementType.values();
        String[] categories = new String[types.length];
        int selectedPos=1;
        for(int i=0; i< types.length; i++){
            categories[i] = MovementType.itString(types[i]);
            if( categories[i].equals(selectedCategory) ){
                selectedPos = i;
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.general_spinner, categories);
        categoryCB.setAdapter( adapter );
        categoryCB.setSelection( selectedPos );
    }

    private void loadDescription(String description){
        descriptionTB = (EditText) findViewById(R.id.descriptionTB);
        descriptionTB.setText(description);
    }

    private void initVocalCommandsBtn() {
        speakBtn = (ImageButton) findViewById(R.id.respeakBtn);
        speakBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.startVocalCommandListening();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.insertion_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.insertTransactionBtn) {

            if(checkEmptyFields()){
                AlertDialog dialog = controller.createInfoDialog("Informazioni mancanti", "Per poter procedere al salvataggio della transazione è necessario valorizzare tutti i campi", R.drawable.danger_orange);
                dialog.show();
                return true;
            }

            boolean savingResult = controller.saveMovement(datePkr.getText().toString(),
                                                            timePkr.getText().toString(),
                                                            amountTB.getText().toString(),
                                                            categoryCB.getSelectedItem().toString(),
                                                            descriptionTB.getText().toString());
            if( !savingResult ){
                AlertDialog dialog = controller.createInfoDialog("Salvataggio non riuscito", "Si è verificato un problema durante il salvataggio della transazione. Riprovare.", R.drawable.danger_red);
                dialog.show();
                return true;
            }

            controller.startMainActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean checkEmptyFields(){
        if("".equals(datePkr.getText().toString()) ||
            "".equals(timePkr.getText().toString()) ||
            "".equals(amountTB.getText().toString()) ||
            "".equals(categoryCB.getSelectedItem().toString()) ||
            "".equals(descriptionTB.getText().toString()) ){
            return true;
        }
        return false;
    }

    /**
     * Receiving speech input
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    updateMovementData( result.get(0) );
                }
                break;
            }
        }
    }

    private void updateMovementData(String speechText){

        Speech speech = controller.processVocalInsertion(speechText);

        if(speech == null){
            AlertDialog dialog = controller.createVocalOperationErrorDialog("Comando vocale non valido", "Solo i comandi vocali relativi all'<b>inserimento</b> sono ammessi in questa pagina.<br/><br/>Vuoi leggere la guida dei comandi vocali per imparare come utilizzarli?");
            dialog.show();
        }else{

            //Aggiorno la data
            if( speech.get(ContentType.DATE) != "" ){
                loadDate( speech.get(ContentType.DATE) );
            }
            //Aggiorno l'ora
            if( speech.get(ContentType.TIME) != "" ){
                loadTime( speech.get(ContentType.TIME) );
            }
            //Aggiorno l'importo
            if( speech.get(ContentType.AMOUNT) != "" ){
                amountTB.setText( speech.get(ContentType.AMOUNT) );
            }
            //Aggiorno la categoria
            if( speech.get(ContentType.CATEGORY) != "" ){
                loadCategory( speech.get(ContentType.CATEGORY) );
            }
            //Aggiorno la descrizione
            if( speech.get(ContentType.DESCRIPTION) != "" ){
                descriptionTB.setText( speech.get(ContentType.DESCRIPTION) );
            }
        }
    }
}