package com.jaewa.jakebovoice.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jaewa.jakebovoice.R;

import static com.jaewa.jakebovoice.R.layout.vocal_guide_activity;

/**
 * Created by cristian on 14/10/17.
 */

public class VocalGuideActivity extends AppCompatActivity {

    private TextView intro;
    private RelativeLayout insertionGuideBtn;
    private RelativeLayout searchGuideBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(vocal_guide_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadIntro();
        initInsertionGuideBtn();
        initSearchGuideBtn();
    }

    private void loadIntro() {

        intro = (TextView) findViewById(R.id.vocalGuideIntroText);
        intro.setText( fromHtml( "Tramite i comandi vocali è possibile <b>registrare nuove spese o entrate economiche</b> " +
                                "all'interno di Jakebo, ed <b>effettuare ricerche tra le voci inserite</b> per ottenere un " +
                                "resoconto completo di quanto è stato speso, in un determinato arco di tempo, per ciascuna categoria di spesa.<br><br>" +
                                "Seleziona un tipo di operazione per visualizzare i suoi <b>comandi specifici</b> ed alcuni <b>esempi d'uso</b>. " ));
    }

    private void initInsertionGuideBtn() {
        insertionGuideBtn = (RelativeLayout) findViewById(R.id.insertionGuideBtn);
        insertionGuideBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), InsertionGuideActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initSearchGuideBtn() {
        searchGuideBtn = (RelativeLayout) findViewById(R.id.searchGuideBtn);
        searchGuideBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SearchGuideActivity.class);
                startActivity(intent);
            }
        });
    }

    @SuppressWarnings("deprecation")
    private static Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
}
