package com.jaewa.jakebovoice.view;

import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by cristian on 14/10/17.
 */

public class InsertionGuideActivity extends OperationGuideActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addExample("<i>\"<u>Inserisci</u> <u>importo</u> 1270 euro, <u>categoria</u> entrata, <u>descrizione</u> stipendio ottobre\"</i>", true);
        addExample("<i>\"<u>Aggiungi</u> <u>data</u> 3 gennaio 2017, <u>ora</u> unidici e mezzo, <u>categoria</u> inaspettata, <u>descrizione</u> autovelox, <u>importo</u> 54,25 €\"</i>", false);
        addExample("<i>\"<u>Segna</u> <u>data</u> ieri, <u>categoria</u> hobby, <u>descrizione</u> biglietto del cinema, <u>importo</u> sette euro e cinquanta\"</i>", true);
        addExample("<i>\"<u>Inserisci</u> <u>importo</u> dieci euro, <u>categoria</u> sussistenza, <u>descrizione</u> benzina auto\"</i>", false);
        addExample("<i>\"<u>Inserisci</u> <u>data</u> sabato scorso, <u>importo</u> venticinque euro, <u>categoria</u> hobby, <u>descrizione</u> ristorante giapponese\"</i>", true);
        addExample("<i>\"<u>Registra</u> <u>data</u> martedì, <u>importo</u> trentaquattro e venti, <u>categoria</u> inaspettata, <u>descrizione</u> ruota bucata\"</i>", false);
    }

    @Override
    protected String opStart() {
        return "<b>Per avviare l'inserimento</b> di una nuova transazione pronuncia una delle seguenti parole:";
    }

    @Override
    protected String opKeywordsList() {
        return "<i><u>inserisci</u></i>, <i><u>aggiungi</u></i>, <i><u>registra</u></i>, <i><u>segna</u></i>";
    }

    @Override
    protected String opEnd() {
        return "quindi, per ogni informazione che vuoi associare alla transazione, pronuncia il <b>tipo di " +
                "informazione</b> da associare, seguito dall'<b>informazione vera e propria</b>.<br/><br/>" +
                "È possibile inserire i vari tipi di informazione nell'ordine che si preferisce.";
    }

    @Override
    protected String cmdStart() {
        return "I <b>tipi di informazione</b> che è possibile associare ad un inserimento sono:";
    }

    @Override
    protected String cmdKeywordsList() {
        return "<i><u>data</u></i>, <i><u>ora</u></i>, <i><u>importo</u></i>, <i><u>categoria</u></i>, <i><u>descrizione</u></i>";
    }

    @Override
    protected String cmdEnd() {
        return "Di seguito sono riportati <b>alcuni esempi</b>:";
    }
}
