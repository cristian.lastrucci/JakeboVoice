package com.jaewa.jakebovoice.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.jaewa.jakebovoice.R;
import com.jaewa.jakebovoice.model.MovementType;
import com.jaewa.jakebovoice.speechParser.ContentType;
import com.jaewa.jakebovoice.speechParser.PeriodParser;

import java.util.Map;

import static com.jaewa.jakebovoice.R.layout.search_params_activity;

/**
 * Created by cristian on 04/10/17.
 */

public class SearchParamsActivity extends AppCompatActivity {

    private Spinner periodCB;
    private EditText fromDateTB;
    private EditText toDateTB;
    private Spinner categoryCB;
    private Button searchBtn;
    private Button cancelBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(search_params_activity);

        loadData();
    }

    private void loadData() {
        Intent intent = getIntent();

        loadPeriod( intent.getStringExtra("periodFrom"), intent.getStringExtra("periodTo") );
        loadCategory( intent.getStringExtra("category") );
        initSearchBtn();
        initCancelBtn();
    }

    private void loadPeriod(String fromDate, String toDate ) {

        intiPeriodCB();

        String[] periods = new String[]{"Sempre", "Da ... A ...", "Settimana scorsa", "Settimana corrente", "Settimana prossima", "Mese scorso", "Mese corrente", "Mese prossimo", "Anno scorso", "Anno corrente", "Anno prossimo"};
        int selectedPos;
        if(fromDate != null && !"".equals(fromDate) && toDate != null && !"".equals(toDate)){
            selectedPos = 1;
        }else{
            selectedPos = 0;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.general_spinner, periods);
        periodCB.setAdapter( adapter );
        periodCB.setSelection( selectedPos );

        fromDateTB = (EditText) findViewById(R.id.fromDateTB);
        fromDateTB.setText( fromDate != null ? fromDate : "" );
        fromDateTB.setEnabled( selectedPos == 1 ? true : false );
        fromDateTB.setBackgroundResource( selectedPos == 1 ? R.drawable.rounderedborder : R.drawable.disabled_field );

        toDateTB = (EditText) findViewById(R.id.toDateTB);
        toDateTB.setText( toDate != null ? toDate : "" );
        toDateTB.setEnabled( selectedPos == 1 ? true : false );
        toDateTB.setBackgroundResource( selectedPos == 1 ? R.drawable.rounderedborder : R.drawable.disabled_field );
    }

    private void intiPeriodCB() {
        periodCB = (Spinner) findViewById(R.id.periodFilterCB);

        periodCB.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (id == 0){
                    fromDateTB.setText(null);
                    fromDateTB.setEnabled(false);
                    fromDateTB.setBackgroundResource(R.drawable.disabled_field);
                    toDateTB.setText(null);
                    toDateTB.setEnabled(false);
                    toDateTB.setBackgroundResource(R.drawable.disabled_field);
                }else if(id == 1){
                    fromDateTB.setEnabled(true);
                    fromDateTB.setBackgroundResource(R.drawable.rounderedborder);
                    toDateTB.setEnabled(true);
                    toDateTB.setBackgroundResource(R.drawable.rounderedborder);
                }else{
                    PeriodParser periodParser = new PeriodParser();
                    Map<ContentType, String> periodBoundaries = periodParser.parse( periodCB.getSelectedItem().toString() );
                    fromDateTB.setText(periodBoundaries.get(ContentType.DATE_FROM));
                    fromDateTB.setEnabled(false);
                    fromDateTB.setBackgroundResource(R.drawable.disabled_field);
                    toDateTB.setText(periodBoundaries.get(ContentType.DATE_TO));
                    toDateTB.setEnabled(false);
                    toDateTB.setBackgroundResource(R.drawable.disabled_field);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadCategory( String selectedCategory) {
        categoryCB = (Spinner) findViewById(R.id.categoryFilterCB);
        MovementType[] types = MovementType.values();
        String[] categories = new String[ 1 + types.length ];
        categories[0] = "Tutte";
        int selectedPos=0;
        for(int i=0; i< types.length; i++){
            categories[i+1] = MovementType.itString(types[i]);
            if( categories[i+1].equals(selectedCategory) ){
                selectedPos = i+1;
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.general_spinner, categories);
        categoryCB.setAdapter( adapter );
        categoryCB.setSelection( selectedPos );
    }

    private void initSearchBtn() {
        searchBtn = (Button) findViewById(R.id.searchParamsSearchBtn);

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean always = false;
                if("".equals(fromDateTB.getText().toString()) || "".equals(toDateTB.getText().toString())){
                    always = true;
                }

                Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
                intent.putExtra( "periodFrom", always ? "sempre" : fromDateTB.getText().toString() );
                intent.putExtra( "periodTo", always ? "sempre" : toDateTB.getText().toString() );
                intent.putExtra( "category", categoryCB.getSelectedItem().toString() );
                startActivityIfNeeded(intent, 0);
            }
        });
    }

    private void initCancelBtn() {
        cancelBtn = (Button) findViewById(R.id.searchParamsCancelBtn);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}