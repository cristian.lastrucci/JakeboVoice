package com.jaewa.jakebovoice.view;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.jaewa.jakebovoice.R;
import com.jaewa.jakebovoice.controller.SearchController;
import com.jaewa.jakebovoice.dataStorage.DbUtils;
import com.jaewa.jakebovoice.speechParser.AmountParser;
import com.jaewa.jakebovoice.speechParser.ContentType;
import com.jaewa.jakebovoice.speechParser.Speech;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static android.R.attr.category;
import static com.jaewa.jakebovoice.R.layout.search_activity;
import static com.jaewa.jakebovoice.speechParser.DateUtils.formatDate;

/**
 * Created by cristian on 26/09/17.
 */

public class SearchActivity extends AppCompatActivity {

    private SearchController controller;
    private final int REQ_CODE_SPEECH_INPUT = 100;

    private String fromDate;
    private String toDate;
    private TextView periodL;
    private TextView categoryL;
    private ImageButton searchSpeakBtn;
    private ImageButton balanceBtn;
    private LinearLayout balanceValueContainer;
    private ImageButton totalsByCategoryBtn;
    private LinearLayout totalsByCategoryList;
    private ImageButton searchResultBtn;
    private LinearLayout searchResultsList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(search_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        controller = new SearchController(this, getApplicationContext());

        loadData();

        findMovements();
    }

    private void loadData() {
        Intent intent = getIntent();
        loadPeriod(intent.getStringExtra("periodFrom"), intent.getStringExtra("periodTo"));
        loadCategory(intent.getStringExtra("category"));

        initVocalCommandsBtn();
        initBalanceArea();
        initTotalsByCategoryArea();
        initSearchResultsArea();
    }

    private void loadPeriod(String f, String t) {
        periodL = (TextView) findViewById(R.id.periodFilterSelection);
        fromDate = f;
        toDate = t;
        if(fromDate != null && "sempre".equals(fromDate) && toDate != null && "sempre".equals(toDate)) {
            periodL.setText( "Sempre" );
        }else if(fromDate != null && !"".equals(fromDate) && toDate != null && !"".equals(toDate)){
            periodL.setText( fromDate + " - " + toDate );
        } else{
            Calendar from = Calendar.getInstance();
            from.set(Calendar.DAY_OF_MONTH, 1);
            fromDate = formatDate(from);
            Calendar to = Calendar.getInstance();
            to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));
            toDate = formatDate(to);
            periodL.setText(formatDate(from) + " - " + formatDate(to));
        }
    }

    private void loadCategory(String category) {
        categoryL = (TextView) findViewById(R.id.categoryFilterSelection);
        categoryL.setText( category != null && !"".equals(category) ? category : "Tutte" );
    }

    private void initVocalCommandsBtn() {
        searchSpeakBtn = (ImageButton) findViewById(R.id.searchSpeakBtn);
        searchSpeakBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.startVocalCommandListening();
            }
        });
    }

    private void initBalanceArea() {
        balanceValueContainer = (LinearLayout) findViewById(R.id.balanceValueContainer);
        balanceBtn = (ImageButton) findViewById(R.id.balanceBtn);
        balanceBtn.setOnClickListener( getAccordionListener(balanceValueContainer, balanceBtn) );
    }

    private void initTotalsByCategoryArea() {
        totalsByCategoryList = (LinearLayout) findViewById(R.id.totalsByCategoryList);
        totalsByCategoryBtn = (ImageButton) findViewById(R.id.totalsByCategoryBtn);
        totalsByCategoryBtn.setOnClickListener( getAccordionListener(totalsByCategoryList, totalsByCategoryBtn) );
    }

    private void initSearchResultsArea() {
        searchResultsList = (LinearLayout) findViewById(R.id.searchResultsList);
        searchResultBtn = (ImageButton) findViewById(R.id.searchResultBtn);
        searchResultBtn.setOnClickListener( getAccordionListener(searchResultsList, searchResultBtn) );
    }

    private View.OnClickListener getAccordionListener(final LinearLayout content, final ImageButton btn){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(content.getVisibility() == View.VISIBLE){
                    content.setVisibility(View.GONE);
                    btn.setImageResource(R.drawable.below_icon);
                }else{
                    content.setVisibility(View.VISIBLE);
                    btn.setImageResource(R.drawable.up_icon);
                }
            }
        };
    }

    private void findMovements() {

        searchResultsList.removeAllViews();
        Map<String, Float> totals = new HashMap<>();

        controller.openDBConnection();
        Cursor cursor = controller.findMovements(periodL.getText().toString(), fromDate, toDate, categoryL.getText().toString());

        int i=0;
        while (cursor.moveToNext()){

            String[] datetime = cursor.getString( cursor.getColumnIndex(DbUtils.FIELD_DATETIME) ).split(" ");
            String[] d = datetime[0].split("-");
            String date = d[2] + "/" + d[1] + "/" + d[0];
            String time = datetime[1];
            String amount = cursor.getString( cursor.getColumnIndex(DbUtils.FIELD_AMOUNT) );
            String category = cursor.getString( cursor.getColumnIndex(DbUtils.FIELD_CATEGORY) );
            String description = cursor.getString( cursor.getColumnIndex(DbUtils.FIELD_DESCRIPTION) );

            searchResultsList.addView( createMovementContainer(i%2==0, date, time, amount, category, description) );

            if(totals.containsKey(category)){
                totals.put(category, totals.get(category) + Float.parseFloat(amount));
            }else{
                totals.put(category, Float.parseFloat(amount));
            }

            i++;
        }
        controller.closeDBConnection();

        updateTotalsList(totals);

        updateBalance(totals);

    }

    private LinearLayout createMovementContainer(boolean evenPosition, String date, String time, String amount, String category, String description) {

        LayoutParams movementContainerParams = new LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        movementContainerParams.setMargins(dp(0), dp(0), dp(0), dp(0));

        LinearLayout movementContainer = new LinearLayout(getApplicationContext());
        movementContainer.setOrientation(LinearLayout.HORIZONTAL);
        movementContainer.setLayoutParams(movementContainerParams);
        movementContainer.setWeightSum(3);

        int backgroundId = evenPosition==true ? R.drawable.simple_border : R.drawable.gray_area;

        movementContainer.addView( createDatetimeContainer(backgroundId, date, time) );
        movementContainer.addView( createDataContainer(backgroundId, amount, category, description) );

        return movementContainer;
    }

    private LinearLayout createDatetimeContainer(int backgroundId, String date, String time) {

        LayoutParams datetimeContainerParams = new LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);

        LinearLayout datetimeContainer = new LinearLayout(getApplicationContext());
        datetimeContainer.setLayoutParams(datetimeContainerParams);

        datetimeContainer.setBackgroundResource(backgroundId);
        datetimeContainer.setPadding(dp(7), dp(25), dp(7), dp(7));
        datetimeContainer.setOrientation(LinearLayout.VERTICAL);
        datetimeContainer.setWeightSum(1);

        LayoutParams datetimeInternalContainerParams = new LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        datetimeInternalContainerParams.gravity = Gravity.CENTER;
        LinearLayout datetimeInternalContainer = new LinearLayout(getApplicationContext());
        datetimeInternalContainer.setLayoutParams(datetimeInternalContainerParams);
        datetimeInternalContainer.setOrientation(LinearLayout.VERTICAL);

        ImageView datetimeIcon = new ImageView( getApplicationContext() );
        LayoutParams datetimeIconParams = new LayoutParams(dp(20), dp(20));
        datetimeIconParams.gravity = Gravity.CENTER_HORIZONTAL;
        datetimeIcon.setLayoutParams(datetimeIconParams);
        datetimeIcon.setImageResource( R.drawable.date_icon );

        TextView dateTw = new TextView(getApplicationContext());
        LayoutParams dateTwParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        dateTwParams.gravity = Gravity.CENTER_HORIZONTAL;
        dateTw.setLayoutParams(dateTwParams);
        dateTw.setTextColor( Color.parseColor("#000000") );
        dateTw.setTextSize(dp(10));
        dateTw.setText(date);

        TextView timeTw = new TextView(getApplicationContext());
        LayoutParams timeTwParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        timeTwParams.gravity = Gravity.CENTER_HORIZONTAL;
        timeTw.setLayoutParams(timeTwParams);
        timeTw.setTextColor( Color.parseColor("#000000") );
        timeTw.setTextSize(dp(10));
        timeTw.setText(time);

        datetimeInternalContainer.addView(datetimeIcon);
        datetimeInternalContainer.addView(dateTw);
        datetimeInternalContainer.addView(timeTw);

        datetimeContainer.addView(datetimeInternalContainer);

        return datetimeContainer;
    }

    private LinearLayout createDataContainer(int backgroundId, String amount, String category, String description) {

        LayoutParams dataContainerParams = new LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        LinearLayout dataContainer = new LinearLayout(getApplicationContext());
        dataContainer.setLayoutParams(dataContainerParams);
        dataContainer.setOrientation(LinearLayout.VERTICAL);
        dataContainer.setWeightSum(2);

        dataContainer.addView( createSingleDataContainer(amount,backgroundId,  R.drawable.amount_icon) );
        dataContainer.addView( createSingleDataContainer(category, backgroundId, R.drawable.category_icon) );
        dataContainer.addView( createSingleDataContainer(description, backgroundId, R.drawable.description_icon) );

        return dataContainer;
    }

    private LinearLayout createSingleDataContainer(String content, int backgroundId, int icon) {
        LayoutParams singleDataContainerParams = new LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        LinearLayout singleDataContainer = new LinearLayout(getApplicationContext());
        singleDataContainer.setLayoutParams(singleDataContainerParams);

        singleDataContainer.setBackgroundResource(backgroundId);
        singleDataContainer.setPadding(dp(7), dp(7), dp(7), dp(7));
        singleDataContainer.setOrientation(LinearLayout.HORIZONTAL);

        ImageView dataIcon = new ImageView( getApplicationContext() );
        LayoutParams dataIconParams = new LayoutParams(dp(20), dp(20));
        dataIconParams.setMargins(dp(0), dp(0), dp(10), dp(0));
        dataIconParams.gravity = Gravity.CENTER_VERTICAL;
        dataIcon.setLayoutParams(dataIconParams);
        dataIcon.setImageResource( icon );

        TextView dataTw = new TextView(getApplicationContext());
        LayoutParams dataTwParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        dataTwParams.gravity = Gravity.CENTER_VERTICAL;
        dataTw.setLayoutParams(dataTwParams);
        dataTw.setTextColor( Color.parseColor("#000000") );
        dataTw.setTextSize(dp(10));
        dataTw.setText(content);

        singleDataContainer.addView(dataIcon);
        singleDataContainer.addView(dataTw);

        return singleDataContainer;

    }

    private void updateTotalsList(Map<String, Float> totals) {

        totalsByCategoryList.removeAllViews();

        int i = 0;
        for(String category : totals.keySet()){
            totalsByCategoryList.addView( createTotalContainer(category, totals.get(category), i%2==0) );
            i++;
        }

    }

    private LinearLayout createTotalContainer(String category, Float amount, boolean evenPosition) {

        LayoutParams totalContainerParams = new LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        totalContainerParams.setMargins(dp(0), dp(0), dp(0), dp(0));

        LinearLayout totalContainer = new LinearLayout(getApplicationContext());
        totalContainer.setOrientation(LinearLayout.HORIZONTAL);
        totalContainer.setLayoutParams(totalContainerParams);
        totalContainer.setWeightSum(3);

        int backgroundId = evenPosition==true ? R.drawable.simple_border : R.drawable.gray_area;

        totalContainer.addView( createLabelContainer(backgroundId, category, dp(120)) );
        totalContainer.addView( createLabelContainer(backgroundId, AmountParser.formatAmount(amount) + " €", LayoutParams.MATCH_PARENT) );

        return totalContainer;
    }

    private LinearLayout createLabelContainer(int backgroundId, String label, int width) {
        LayoutParams labelContainerParams = new LayoutParams(width, LayoutParams.MATCH_PARENT);

        LinearLayout labelContainer = new LinearLayout(getApplicationContext());
        labelContainer.setLayoutParams(labelContainerParams);

        labelContainer.setBackgroundResource(backgroundId);
        labelContainer.setPadding(dp(7), dp(7), dp(7), dp(7));
        labelContainer.setOrientation(LinearLayout.VERTICAL);
        labelContainer.setWeightSum(1);

        TextView labelTw = new TextView(getApplicationContext());
        LayoutParams labelTwParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        labelTwParams.gravity = Gravity.CENTER_HORIZONTAL;
        labelTwParams.setMargins(dp(0), dp(0), dp(0), dp(5));
        labelTw.setLayoutParams(labelTwParams);
        labelTw.setTextColor( Color.parseColor("#000000") );
        labelTw.setTextSize(dp(10));
        labelTw.setText(label);

        labelContainer.addView(labelTw);

        return labelContainer;
    }

    private void updateBalance(Map<String, Float> totals) {
        balanceValueContainer.removeAllViews();
        Float balance = 0f;
        for(String category : totals.keySet()){
            if("Entrata".equals(category)){
                balance = balance + totals.get(category);
            }else{
                balance = balance - totals.get(category);
            }
        }

        balanceValueContainer.addView( createBalanceContainer(balance) );
    }

    private TextView createBalanceContainer(Float balance) {
        TextView balanceTw = new TextView(getApplicationContext());
        LayoutParams balanceTwParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        balanceTwParams.gravity = Gravity.CENTER_HORIZONTAL;
        balanceTw.setLayoutParams(balanceTwParams);
        balanceTw.setTextColor( balance>=0 ? Color.parseColor("#13cc00") : Color.parseColor("#ee0000") );
        balanceTw.setTextSize(dp(20));
        balanceTw.setText( AmountParser.formatAmount(balance) + " €" );
        return balanceTw;
    }

    private int dp(float value){
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, getResources().getDisplayMetrics()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.searchTransactionsBtn) {
            controller.startSearchParamsActivity(fromDate, toDate, categoryL.getText().toString());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Receiving speech input
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    Speech speech = controller.processVocalSearch( result.get(0) );
                    if (speech == null){
                        AlertDialog dialog = controller.createVocalOperationErrorDialog("Comando vocale non valido", "Solo i comandi vocali relativi alla <b>ricerca</b> sono ammessi in questa pagina.<br/><br/>Vuoi leggere la guida dei comandi vocali per imparare come utilizzarli?");
                        dialog.show();
                    }else{

                        loadPeriod(speech.get(ContentType.DATE_FROM), speech.get(ContentType.DATE_TO) );
                        loadCategory(speech.get(ContentType.CATEGORY));

                        findMovements();
                    }
                }
                break;
            }
        }
    }
}