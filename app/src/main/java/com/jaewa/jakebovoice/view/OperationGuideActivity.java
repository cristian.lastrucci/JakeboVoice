package com.jaewa.jakebovoice.view;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jaewa.jakebovoice.R;

import static com.jaewa.jakebovoice.R.layout.operation_guide_activity;

/**
 * Created by cristian on 25/10/17.
 */

public abstract class OperationGuideActivity extends AppCompatActivity {

    private LinearLayout operationGuideContainer;
    private TextView opStartP;
    private TextView opKeywordsList;
    private TextView opEndP;
    private TextView cmdStartP;
    private TextView cmdKeywordsList;
    private TextView cmdEndP;

    protected abstract String opStart();
    protected abstract String opKeywordsList();
    protected abstract String opEnd();
    protected abstract String cmdStart();
    protected abstract String cmdKeywordsList();
    protected abstract String cmdEnd();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(operation_guide_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        opStartP = (TextView) findViewById(R.id.operationGuideFirstP);
        opStartP.setText( fromHtml( opStart() ));

        opKeywordsList = (TextView) findViewById(R.id.operationKeywordsList);
        opKeywordsList.setText( fromHtml( opKeywordsList() ));

        opEndP = (TextView) findViewById(R.id.operationGuideSecondP);
        opEndP.setText( fromHtml( opEnd() ));

        cmdStartP = (TextView) findViewById(R.id.operationGuideThirdP);
        cmdStartP.setText( fromHtml( cmdStart() ));

        cmdKeywordsList = (TextView) findViewById(R.id.infoKeywordsList);
        cmdKeywordsList.setText( fromHtml( cmdKeywordsList() ));

        cmdEndP = (TextView) findViewById(R.id.operationGuideFourthP);
        cmdEndP.setText( fromHtml( cmdEnd() ));

        operationGuideContainer = (LinearLayout) findViewById(R.id.operationGuideContainer);
    }

    protected void addExample(String example, boolean evenPosition) {
        LinearLayout.LayoutParams exampleContainerParams = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        exampleContainerParams.setMargins(dp(0), dp(10), dp(0), dp(10));

        LinearLayout exampleContainer = new LinearLayout(getApplicationContext());
        exampleContainer.setOrientation(LinearLayout.VERTICAL);
        exampleContainer.setLayoutParams(exampleContainerParams);
        exampleContainer.setPadding(dp(15), dp(10), dp(15), dp(40));

        int backgroundId = evenPosition==true ? R.drawable.bubble_left_green : R.drawable.bubble_right_green;
        exampleContainer.setBackgroundResource(backgroundId);

        TextView exampleTw = new TextView(getApplicationContext());
        LinearLayout.LayoutParams exampleTwParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        exampleTwParams.gravity = Gravity.CENTER_HORIZONTAL;
        exampleTw.setLayoutParams(exampleTwParams);
        exampleTw.setTextColor( Color.parseColor("#00411c") );
        exampleTw.setTextSize(dp(11));
        exampleTw.setText(fromHtml(example));

        exampleContainer.addView(exampleTw);

        operationGuideContainer.addView( exampleContainer );
    }

    private int dp(float value){
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, getResources().getDisplayMetrics()));
    }

    @SuppressWarnings("deprecation")
    private static Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }
}
