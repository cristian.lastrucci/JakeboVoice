package com.jaewa.jakebovoice.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jaewa.jakebovoice.R;
import com.jaewa.jakebovoice.controller.MainController;
import com.jaewa.jakebovoice.speechParser.AmountParser;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by cristian on 19/09/17.
 */

public class MainActivity extends AppCompatActivity {

    private MainController controller;
    private final int REQ_CODE_SPEECH_INPUT = 100;

    private TextView monthLabel;
    private TextView incomeValue;
    private TextView outflowValue;
    private TextView savingsValue;
    private TextView availableValue;
    private ImageButton speakBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        controller = new MainController(this, getApplicationContext());

        loadData();
        initVocalCommandsBtn();
    }

    private void loadData(){

        monthLabel = (TextView) findViewById(R.id.monthLabel);
        monthLabel.setText( controller.getCurrentMonth() );

        loadMonthIndicators();
    }

    private void loadMonthIndicators() {

        incomeValue = (TextView) findViewById(R.id.incomeValue);
        outflowValue = (TextView) findViewById(R.id.outflowValue);
        savingsValue = (TextView) findViewById(R.id.savingsValue);
        availableValue = (TextView) findViewById(R.id.availableValue);

        Map<String, Float> values = controller.calculateMonthIndicators();

        incomeValue.setText(AmountParser.formatAmount(values.get("monthIncome")) + " €");
        outflowValue.setText(AmountParser.formatAmount(values.get("monthOutflow")) + " €");
        savingsValue.setText(AmountParser.formatAmount(values.get("savings")) + " €");
        availableValue.setText(AmountParser.formatAmount(values.get("available")) + " €");

        availableValue.setTextColor(values.get("available")>=0 ? Color.parseColor("#13cc00") : Color.parseColor("#ee0000") );
    }

    private void initVocalCommandsBtn() {
        speakBtn = (ImageButton) findViewById(R.id.speakBtn);
        speakBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.startVocalCommandListening();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.addBtn) {
            controller.startInsertionActivity();
            return true;
        }else if (id == R.id.searchBtn) {
            controller.startSearchActivity();
            return true;
        }else if (id == R.id.vocalGuideBtn) {
            controller.startVocalGuideActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Receive and process speech input
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    controller.processVoiceCommand( result.get(0) );
                }
                break;
            }
        }
    }
}