package com.jaewa.jakebovoice.view;

import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by cristian on 14/10/17.
 */

public class SearchGuideActivity extends OperationGuideActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addExample("<i>\"<u>Cerca</u> <u>periodo</u> settimana corrente, <u>categoria</u> entrata\"</i>", true);
        addExample("<i>\"<u>Filtra</u> <u>periodo</u> mese scorso\"</i>", false);
        addExample("<i>\"<u>Seleziona</u> <u>periodo</u> da 24 ottobre 2017 a 15 agosto 2018, <u>categoria</u> sussistenza\"</i>", true);
        addExample("<i>\"<u>Cerca</u> <u>periodo</u> maggio, <u>categoria</u> hobby\"</i>", false);
        addExample("<i>\"<u>Cerca</u> <u>periodo</u> 11 ottobre 2007\"</i>", true);
        addExample("<i>\"<u>Trova</u> <u>categoria</u> sussistenza\"</i>", false);
    }

    @Override
    protected String opStart() {
        return "<b>Per avviare la ricerca</b> di un gruppo di transazioni pronuncia una delle seguenti parole:";
    }

    @Override
    protected String opKeywordsList() {
        return "<i><u>cerca</u></i>, <i><u>trova</u></i>, <i><u>filtra</u></i>, <i><u>recupera</u></i>, <i><u>seleziona</u></i>";
    }

    @Override
    protected String opEnd() {
        return "quindi, pronuncia i criteri di ricerca, indicando per ciascuno di essi " +
                "il <b>criterio</b> da aggiungere, seguito dal <b>valore</b> da associare a quest'ultimo.<br/><br/>" +
                "È possibile inserire i vari criteri di ricerca nell'ordine che si preferisce.";
    }

    @Override
    protected String cmdStart() {
        return "I <b>criteri</b> per i quali è possibile effettuare una ricerca sono:";
    }

    @Override
    protected String cmdKeywordsList() {
        return "<i><u>periodo</u></i> e <i><u>categoria</u></i>";
    }

    @Override
    protected String cmdEnd() {
        return "Se il periodo non viene specificato la ricerca sarà compiuta per il mese corrente; mentre se la " +
                "categoria non è specificata la ricerca comprenderà tutte le categorie.<br/><br/>" +
                "Di seguito sono riportati <b>alcuni esempi</b>:";
    }
}