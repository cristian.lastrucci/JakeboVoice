package com.jaewa.jakebovoice.speechParser;

/**
 * Created by cristian on 21/09/17.
 */

public enum ParserType {
    INSERTION, SEARCH, PERIOD, DATE, TIME, AMOUNT, CATEGORY, DESCRIPTION;
}
