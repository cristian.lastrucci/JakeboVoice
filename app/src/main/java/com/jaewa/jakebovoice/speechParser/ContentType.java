package com.jaewa.jakebovoice.speechParser;

/**
 * Created by cristian on 05/10/17.
 */

public enum ContentType {
    OPERATION, DATE_FROM, DATE_TO, DATE, TIME, AMOUNT, CATEGORY, DESCRIPTION;
}
