package com.jaewa.jakebovoice.speechParser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.jaewa.jakebovoice.speechParser.DateUtils.dayOfWeek;
import static com.jaewa.jakebovoice.speechParser.DateUtils.formatDate;
import static com.jaewa.jakebovoice.speechParser.DateUtils.formatSingleDigit;
import static com.jaewa.jakebovoice.speechParser.DateUtils.monthOfYear;

/**
 * Created by cristian on 21/09/17.
 */

public class DateParser extends SpeechParser<Date> {

    private static DateFormat dateFormat;

    static {
        dateFormat = new SimpleDateFormat("dd/mm/yyyy", Locale.ITALIAN);
    }

    @Override
    public Map<ContentType, String> parse(String content) {

        try {
            String[] date = content.replace("x", "10").split(" ");
            String formattedDate = "";
            if(date.length == 1){
                formattedDate = parseSingleWordDateFormat( date[0] );
            }else if(date.length == 2){
                formattedDate = parseTwoWordsDateFormat( date[0], date[1] );
            }else if(date.length == 3){
                formattedDate = parseThreeWordsDateFormat( date[0], date[1], date[2] );
            }

            Map<ContentType, String> result = new HashMap<>();
            result.put( ContentType.DATE, formattedDate );

            return result;

        }catch (Exception e){
            throw new IllegalArgumentException( "Unrecognized date: " + content );
        }
    }

    @Override
    public Date convert(String content){
        try {
            String date = parse(content).get(ContentType.DATE);
            return dateFormat.parse(date);
        }catch(Exception e) {
            throw new IllegalArgumentException( "Unrecognized date: " + content );
        }
    }

    private String parseSingleWordDateFormat(String date) {

        String d = date.toLowerCase();
        Calendar calendar = Calendar.getInstance();
        Integer currentDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;

        //Giorno della settimana corrente
        Integer selectedDay = DateUtils.dayOfWeek(d);
        if(selectedDay != null){
            Integer difference = selectedDay - currentDayOfWeek;
            calendar.add(Calendar.DAY_OF_YEAR, difference);
            return DateUtils.formatDate(calendar);
        }

        //Ieri
        if( "ieri".equals(d) ){
            calendar.add(Calendar.DAY_OF_YEAR, -1);
            return DateUtils.formatDate(calendar);
        }

        //Oggi
        if( "oggi".equals(d) ){
            return DateUtils.formatDate(calendar);
        }

        //Domani
        if( "domani".equals(d) ){
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            return DateUtils.formatDate(calendar);
        }

        return "";
    }

    private String parseTwoWordsDateFormat(String s1, String s2) {
        //Giorno della settimana scorsa o prossima (es: "venerdì scorso", "scorso venerdì", "martedì prossimo", "prossimo martedì")

        Integer selectedDay = DateUtils.dayOfWeek( s1.toLowerCase() );
        String week = s2.toLowerCase();
        if(selectedDay == null){
            selectedDay = DateUtils.dayOfWeek( s2.toLowerCase() );
            week = s1.toLowerCase();
        }

        if(selectedDay != null && Arrays.asList("scorso", "scorsa", "prossimo", "prossima").contains(week) ){

            Calendar calendar = Calendar.getInstance();
            Integer currentDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
            Integer difference = selectedDay - currentDayOfWeek;

            String w = week.toLowerCase();
            if( "scorso".equals(w) || "scorsa".equals(w) ){
                difference = difference - 7;
            }else if( "prossimo".equals(w) || "prossima".equals(w) ){
                difference = difference + 7;
            }else{
                return "";
            }

            calendar.add(Calendar.DAY_OF_YEAR, difference);
            return DateUtils.formatDate(calendar);
        }

        return "";
    }

    private String parseThreeWordsDateFormat(String day, String month, String year) {
        //Giorno Mese Anno (es: "11 8 2015", "24 agosto 2017")
        return DateUtils.formatSingleDigit(Integer.parseInt(day)) + "/" + monthOfYear(month) + "/" + year;
    }
}
