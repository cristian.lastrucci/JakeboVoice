package com.jaewa.jakebovoice.speechParser;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cristian on 21/09/17.
 */

public class Speech {

    private String speechText;
    private Map<ContentType, String> content;

    public Speech(){
        content = new HashMap<>();
    }

    public String getSpeechText(){
        return speechText;
    }

    public String get(ContentType contentType){
        String result = content.get(contentType);
        return  result != null ? result : "";
    }

    public void parse(String speech){

        speechText = speech.toLowerCase();

        List<String> words = Arrays.asList( speechText.split(" ") );
        SpeechParser currentParser = null;
        String currentContent = "";

        for( String word : words ){

            if( !SpeechUtils.isKeyword(word) ){
                currentContent = currentContent.equals("") ? word : currentContent + " " + word;
            }else{

                if( currentParser != null ){
                    try {
                        content.putAll( currentParser.parse(currentContent) );
                    }catch (IllegalArgumentException e){
                        System.out.println(e);
                    }
                }
                currentParser = SpeechUtils.getParser(word);
                currentContent = "";

            }
        }
        if( currentParser != null && !"".equals(currentContent) ){
            try{
                content.putAll( currentParser.parse(currentContent) );
            }catch (IllegalArgumentException e){
                System.out.println(e);
            }
        }
    }
}
