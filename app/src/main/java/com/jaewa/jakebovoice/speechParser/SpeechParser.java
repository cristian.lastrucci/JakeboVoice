package com.jaewa.jakebovoice.speechParser;

import java.util.Map;

/**
 * Created by cristian on 21/09/17.
 */

public abstract class SpeechParser<T> {

    public abstract Map<ContentType, String> parse(String content);

    public abstract T convert(String content);
}
