package com.jaewa.jakebovoice.speechParser;

import com.jaewa.jakebovoice.model.MovementType;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cristian on 21/09/17.
 */

public class CategoryParser extends SpeechParser<MovementType> {

    @Override
    public Map<ContentType, String> parse(String content) {

        try{

            String category = content.toLowerCase();

            if( "entrata".equals(category) ){
                category = "Entrata";
            }else if( "sussistenza".equals(category) ){
                category = "Sussistenza";
            }else if( "optional".equals(category) ){
                category = "Optional";
            }else if( "hobby".equals(category) ){
                category = "Hobby";
            }else if( "inaspettata".equals(category) ){
                category = "Inaspettata";
            }else{
                throw new IllegalArgumentException( "Unrecognized category: " + content );
            }

            Map<ContentType, String> result = new HashMap<>();
            result.put(ContentType.CATEGORY, category);
            return result;

        }catch (Exception e){
            throw new IllegalArgumentException( "Unrecognized category: " + content );
        }
    }

    @Override
    public MovementType convert(String content){
        try {
            String category = parse(content).get(ContentType.CATEGORY).toLowerCase();
            MovementType movementType;

            if( "entrata".equals(category) ){
                movementType = MovementType.INCOME;
            }else if( "sussistenza".equals(category) ){
                movementType = MovementType.SUBSISTENCE;
            }else if( "optional".equals(category) ){
                movementType = MovementType.OPTIONAL;
            }else if( "hobby".equals(category) ){
                movementType = MovementType.HOBBY;
            }else if( "inaspettata".equals(category) ){
                movementType = MovementType.ACCIDENT;
            }else{
                throw new IllegalArgumentException( "Unrecognized category: " + content );
            }
            return movementType;
        }catch(Exception e) {
            throw new IllegalArgumentException( "Unrecognized category: " + content );
        }
    }
}
