package com.jaewa.jakebovoice.speechParser;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cristian on 05/10/17.
 */

public class SpeechUtils {

    private static final Map<ParserType, List<String>> keywords;

    static {
        keywords = new HashMap<ParserType, List<String>>();

        keywords.put(ParserType.INSERTION, Arrays.asList("inserisci", "aggiungi", "registra", "segna"));
        keywords.put(ParserType.SEARCH, Arrays.asList("cerca", "trova", "filtra", "individua", "recupera", "seleziona", "analizza", "esamina"));

        keywords.put(ParserType.PERIOD, Arrays.asList("periodo"));
        keywords.put(ParserType.DATE, Arrays.asList("data", "giorno"));
        keywords.put(ParserType.TIME, Arrays.asList("ora", "ore"));
        keywords.put(ParserType.AMOUNT, Arrays.asList("importo"));
        keywords.put(ParserType.CATEGORY, Arrays.asList("categoria"));
        keywords.put(ParserType.DESCRIPTION, Arrays.asList("descrizione"));
    }

    static boolean isKeyword(String word){

        if(getParserType(word) != null){
            return true;
        }
        return false;
    }

    static SpeechParser getParser(String word){

        ParserType parserType = getParserType(word);

        if(ParserType.INSERTION.equals(parserType) || ParserType.SEARCH.equals(parserType)){
            return  new OperationParser(parserType);
        }else if(ParserType.PERIOD.equals(parserType)){
            return  new PeriodParser();
        }else if(ParserType.DATE.equals(parserType)){
            return  new DateParser();
        }else if(ParserType.TIME.equals(parserType)){
            return  new TimeParser();
        }else if(ParserType.AMOUNT.equals(parserType)){
            return  new AmountParser();
        }else if(ParserType.CATEGORY.equals(parserType)){
            return  new CategoryParser();
        }else if(ParserType.DESCRIPTION.equals(parserType)){
            return  new DescriptionParser();
        }
        return null;
    }

    private static ParserType getParserType(String word){

        ParserType result = null;
        for (ParserType type : ParserType.values()){
            if ( keywords.get(type).contains(word) ){
                result = type;
            }
        }

        return result;
    }
}
