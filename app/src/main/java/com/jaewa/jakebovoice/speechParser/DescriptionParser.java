package com.jaewa.jakebovoice.speechParser;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cristian on 21/09/17.
 */

public class DescriptionParser extends SpeechParser<String> {

    @Override
    public Map<ContentType, String> parse(String content) {

        Map<ContentType, String> result = new HashMap<>();
        result.put(ContentType.DESCRIPTION, content);
        return result;
    }

    @Override
    public String convert(String content) {
        return parse(content).get(ContentType.DESCRIPTION);
    }
}
