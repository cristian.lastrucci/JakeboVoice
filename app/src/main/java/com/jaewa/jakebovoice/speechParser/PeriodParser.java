package com.jaewa.jakebovoice.speechParser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.R.id.list;

/**
 * Created by cristian on 04/10/17.
 */

public class PeriodParser extends SpeechParser<List<Date>> {

    private static DateFormat datePeriodFormat;

    static {
        datePeriodFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ITALIAN);
    }

    @Override
    public Map<ContentType, String> parse(String content) {

        try {
            String c = content.replace("x", "10");
            String[] period = c.split(" ");

            List<String> periodBoundaries;

            Calendar from = Calendar.getInstance();
            Calendar to = Calendar.getInstance();

            if(period.length == 1){
                periodBoundaries = parseSingleWordPeriodFormat( period[0], from, to );
            }else if(period.length == 2){
                periodBoundaries = parseTwoWordsPeriodFormat( Arrays.asList(period), from, to );
            }else if(period.length == 3){
                periodBoundaries = parseSingleDatePeriodFormat( c, from, to );
            }else {
                periodBoundaries = parseManyWordsPeriodFormat( c.toLowerCase(), from, to );
            }

            Map<ContentType, String> result = new HashMap<>();
            result.put( ContentType.DATE_FROM, periodBoundaries.get(0) );
            result.put( ContentType.DATE_TO, periodBoundaries.get(1) );

            return result;

        }catch (Exception e){
            throw new IllegalArgumentException( "Unrecognized period: " + content );
        }
    }

    @Override
    public List<Date> convert(String content) {
        try {
            Map<ContentType, String> result = parse(content);
            List<Date> period = new ArrayList<>();
            period.add( datePeriodFormat.parse(result.get(ContentType.DATE_FROM)) );
            period.add( datePeriodFormat.parse(result.get(ContentType.DATE_TO)) );
            return period;
        }catch(Exception e) {
            throw new IllegalArgumentException( "Unrecognized period: " + content );
        }
    }

    private List<String> parseSingleWordPeriodFormat(String period, Calendar from, Calendar to) {

        List<String> periodBoundaries = new ArrayList<>();

        //Sempre
        if ( "sempre".equals(period.toLowerCase()) ){

            periodBoundaries.add( "sempre" );
            periodBoundaries.add( "sempre" );

            return periodBoundaries;
        }

        //Ieri
        if ( "ieri".equals(period.toLowerCase()) ){
            from.add(Calendar.DAY_OF_MONTH, -1);
            to.add(Calendar.DAY_OF_MONTH, -1);

            periodBoundaries.add( DateUtils.formatDate(from) );
            periodBoundaries.add( DateUtils.formatDate(to) );

            return periodBoundaries;
        }

        //Oggi
        if ( "oggi".equals(period.toLowerCase()) ){

            periodBoundaries.add( DateUtils.formatDate(from) );
            periodBoundaries.add( DateUtils.formatDate(to) );

            return periodBoundaries;
        }

        //Domani
        if ( "domani".equals(period.toLowerCase()) ){
            from.add(Calendar.DAY_OF_MONTH, 1);
            to.add(Calendar.DAY_OF_MONTH, 1);

            periodBoundaries.add( DateUtils.formatDate(from) );
            periodBoundaries.add( DateUtils.formatDate(to) );

            return periodBoundaries;
        }

        //Mese dell'anno corrente
        String month = DateUtils.monthOfYear(period);
        if ( !"".equals(month) ){

            from.set(Calendar.MONTH, Integer.parseInt(month) - 1 );
            from.set(Calendar.DAY_OF_MONTH, 1);

            to.set(Calendar.MONTH, Integer.parseInt(month) - 1 );
            to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));

            periodBoundaries.add( DateUtils.formatDate(from) );
            periodBoundaries.add( DateUtils.formatDate(to) );

            return periodBoundaries;
        }

        //Anno
        try{
            Integer year = Integer.parseInt(period);

            from.set(Calendar.YEAR, year);
            from.set(Calendar.MONTH, 0 );
            from.set(Calendar.DAY_OF_MONTH, 1);

            to.set(Calendar.YEAR, year);
            to.set(Calendar.MONTH, 11 );
            to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));

            periodBoundaries.add( DateUtils.formatDate(from) );
            periodBoundaries.add( DateUtils.formatDate(to) );

            return periodBoundaries;

        }catch (NumberFormatException e){}

        return periodBoundaries;
    }

    private List<String> parseTwoWordsPeriodFormat(List<String> period, Calendar from, Calendar to) {

        List<String> p = new ArrayList<>();
        for(String s : period){
            p.add(s.toLowerCase());
        }

        //{Settimana, Mese, Agosto, Anno} {Scorso, Corrente, Prossimo}
        if ( p.contains("settimana") ){
            if ( p.contains("scorsa") ){
                return parseWeekPeriodFormat( -7, from, to );
            }else if ( p.contains("corrente") || p.contains("questa") ){
                return parseWeekPeriodFormat( 0, from, to );
            }else if ( p.contains("prossima") || p.contains("nuova") ){
                return parseWeekPeriodFormat( 7, from, to );
            }
        }else if ( p.contains("mese") ){
            if ( p.contains("scorso") ){
                return parseMonthPeriodFormat( -1, from, to );
            }else if ( p.contains("corrente") || p.contains("questo") ){
                return parseMonthPeriodFormat( 0, from, to );
            }else if ( p.contains("prossimo") ){
                return parseMonthPeriodFormat( +1, from, to );
            }
        }else if ( p.contains("anno") ){
            if ( p.contains("scorso") ){
                return parseYearPeriodFormat( -1, from, to );
            }else if ( p.contains("corrente") || p.contains("questo") ){
                return parseYearPeriodFormat( 0, from, to );
            }else if ( p.contains("prossimo") ){
                return parseYearPeriodFormat( +1, from, to );
            }
        }

        //Mese Anno
        return parseMonthYearPeriodFormat(p, from, to);
    }

    private List<String> parseWeekPeriodFormat(int shift, Calendar from, Calendar to) {

        List<String> periodBoundaries = new ArrayList<>();

        int leftShift = 1 - from.get(Calendar.DAY_OF_WEEK) + shift + 1;
        int rightShift = 7 - from.get(Calendar.DAY_OF_WEEK) + shift + 1;

        from.add(Calendar.DAY_OF_MONTH, leftShift);
        to.add(Calendar.DAY_OF_MONTH, rightShift);

        periodBoundaries.add( DateUtils.formatDate(from) );
        periodBoundaries.add( DateUtils.formatDate(to) );

        return periodBoundaries;
    }

    private List<String> parseMonthPeriodFormat(int shift, Calendar from, Calendar to) {

        List<String> periodBoundaries = new ArrayList<>();

        from.add(Calendar.MONTH, shift);
        from.set(Calendar.DAY_OF_MONTH, 1);

        to.add(Calendar.MONTH, shift);
        to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));

        periodBoundaries.add( DateUtils.formatDate(from) );
        periodBoundaries.add( DateUtils.formatDate(to) );

        return periodBoundaries;
    }

    private List<String> parseYearPeriodFormat(int shift, Calendar from, Calendar to) {
        List<String> periodBoundaries = new ArrayList<>();

        from.add(Calendar.YEAR, shift);
        from.set(Calendar.MONTH, 0 );
        from.set(Calendar.DAY_OF_MONTH, 1);

        to.add(Calendar.YEAR, shift);
        to.set(Calendar.MONTH, 11 );
        to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));

        periodBoundaries.add( DateUtils.formatDate(from) );
        periodBoundaries.add( DateUtils.formatDate(to) );

        return periodBoundaries;
    }

    private List<String> parseMonthYearPeriodFormat(List<String> period, Calendar from, Calendar to) {

        List<String> periodBoundaries = new ArrayList<>();

        Integer month = Integer.parseInt( DateUtils.monthOfYear(period.get(0)) );
        Integer year = Integer.parseInt(period.get(1));

        from.set(Calendar.YEAR, year );
        from.set(Calendar.MONTH, month - 1 );
        from.set(Calendar.DAY_OF_MONTH, 1);

        to.set(Calendar.YEAR, year );
        to.set(Calendar.MONTH, month - 1 );
        to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));

        periodBoundaries.add( DateUtils.formatDate(from) );
        periodBoundaries.add( DateUtils.formatDate(to) );

        return periodBoundaries;
    }

    private List<String> parseSingleDatePeriodFormat(String period, Calendar from, Calendar to) {

        List<String> periodBoundaries = new ArrayList<>();

        //{data}
        DateParser parser = new DateParser();
        String date = parser.parse(period).get(ContentType.DATE);
        String[] d = date.split("/");

        from.set(Calendar.YEAR, Integer.parseInt(d[2]) );
        from.set(Calendar.MONTH, Integer.parseInt(d[1]) - 1 );
        from.set(Calendar.DAY_OF_MONTH, Integer.parseInt(d[0]) );

        to.set(Calendar.YEAR, Integer.parseInt(d[2]) );
        to.set(Calendar.MONTH, Integer.parseInt(d[1]) - 1 );
        to.set(Calendar.DAY_OF_MONTH, Integer.parseInt(d[0]) );

        periodBoundaries.add( DateUtils.formatDate(from) );
        periodBoundaries.add( DateUtils.formatDate(to) );

        return periodBoundaries;
    }

    private List<String> parseManyWordsPeriodFormat(String period, Calendar from, Calendar to) {

        List<String> periodBoundaries = new ArrayList<>();

        //Da {data} A {data}
        String formattedPeriod = period.replace("dal ", "da ").replace(" al ", " a ");
        String[] list = formattedPeriod.split(" a ");
        String fromText = list[0].replace("da ", "");
        String toText = list[1];

        DateParser parser = new DateParser();
        String dateFrom = parser.parse(fromText).get(ContentType.DATE);
        String[] dF = dateFrom.split("/");
        String dateTo = parser.parse(toText).get(ContentType.DATE);
        String[] dT = dateTo.split("/");

        from.set(Calendar.YEAR, Integer.parseInt(dF[2]) );
        from.set(Calendar.MONTH, Integer.parseInt(dF[1]) - 1 );
        from.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dF[0]) );

        to.set(Calendar.YEAR, Integer.parseInt(dT[2]) );
        to.set(Calendar.MONTH, Integer.parseInt(dT[1]) - 1 );
        to.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dT[0]) );

        if(from.compareTo(to) > 0){
            Calendar date = to;
            to = from;
            from = date;
        }

        periodBoundaries.add( DateUtils.formatDate(from) );
        periodBoundaries.add( DateUtils.formatDate(to) );

        return periodBoundaries;
    }

}
