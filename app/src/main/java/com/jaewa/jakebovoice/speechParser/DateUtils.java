package com.jaewa.jakebovoice.speechParser;

import java.util.Calendar;

/**
 * Created by cristian on 11/10/17.
 */

public class DateUtils {

    public static Integer dayOfWeek(String day){
        String d = day.toLowerCase();
        if( "lunedì".equals(d) ){
            return 1;
        }else if( "martedì".equals(d) ){
            return 2;
        }else if( "mercoledì".equals(d) ){
            return 3;
        }else if( "giovedì".equals(d) ){
            return 4;
        }else if( "venerdì".equals(d) ){
            return 5;
        }else if( "sabato".equals(d) ){
            return 6;
        }else if( "domenica".equals(d) ){
            return 7;
        }
        return null;
    }

    public static Integer dayOfWeek(Calendar date){
        int d = date.get(Calendar.DAY_OF_WEEK);
        if( d == 1 ){
            return 7;
        }else {
            return d-1;
        }
    }

    public static String parseMonth (int month){

        if (month < 0 || month > 11){
            throw new IllegalArgumentException();
        }

        if(Calendar.JANUARY == month){
            return "Gennaio";
        }else if(Calendar.FEBRUARY == month){
            return "Febbraio";
        }else if(Calendar.MARCH == month){
            return "Marzo";
        }else if(Calendar.APRIL == month){
            return "Aprile";
        }else if(Calendar.MAY == month){
            return "Maggio";
        }else if(Calendar.JUNE == month){
            return "Giugno";
        }else if(Calendar.JULY == month){
            return "Luglio";
        }else if(Calendar.AUGUST == month){
            return "Agosto";
        }else if(Calendar.SEPTEMBER == month){
            return "Settembre";
        }else if(Calendar.OCTOBER == month){
            return "Ottobre";
        }else if(Calendar.NOVEMBER == month){
            return "Novembre";
        }else{
            return "Dicembre";
        }
    }

    public static String monthOfYear(String month){
        try{
            Integer m = Integer.valueOf(month);
            if(m<10){
                return "0"+String.valueOf(m);
            }else if(m<12){
                return String.valueOf(m);
            }
            return "";
        }catch (NumberFormatException e){
            String mo = month.toLowerCase();
            if("gennaio".equals(mo)){
                return "01";
            }else if("febbraio".equals(mo)){
                return "02";
            }else if("marzo".equals(mo)){
                return "03";
            }else if("aprile".equals(mo)){
                return "04";
            }else if("maggio".equals(mo)){
                return "05";
            }else if("giugno".equals(mo)){
                return "06";
            }else if("luglio".equals(mo)){
                return "07";
            }else if("agosto".equals(mo)){
                return "08";
            }else if("settembre".equals(mo)){
                return "09";
            }else if("ottobre".equals(mo)){
                return "10";
            }else if("novembre".equals(mo)){
                return "11";
            }else if("dicembre".equals(mo)){
                return "12";
            }
            return "";
        }
    }

    public static String formatSingleDigit(Integer digit){
        return digit<10 ? "0" + String.valueOf(digit) : String.valueOf(digit);
    }

    public static String formatDate(Calendar date){

        Integer year = date.get(Calendar.YEAR);
        Integer month = date.get(Calendar.MONTH) + 1;
        Integer day = date.get(Calendar.DAY_OF_MONTH);

        return formatSingleDigit(day) + "/" +
                formatSingleDigit(month) + "/" +
                String.valueOf(year);

    }

    public static String formatDatetime(Calendar date){

        Integer hour = date.get(Calendar.HOUR_OF_DAY);
        Integer minute = date.get(Calendar.MINUTE);

        return formatDate(date) + " " +
                formatSingleDigit(hour) + ":" +
                formatSingleDigit(minute);

    }

    public static String formatDateForDB(String date) {
        String[] d = date.split("/");
        return d[2] + "-" + d[1] + "-" + d[0];
    }

    public static String formatDateForDB(Calendar date) {
        return String.valueOf(date.get(Calendar.YEAR)) + "-" +
                formatSingleDigit(date.get(Calendar.MONTH)+1) + "-" +
                formatSingleDigit(date.get(Calendar.DAY_OF_MONTH));
    }

    public static String formatDatetimeForDB(String date, String time) {
        String[] d = date.split("/");
        return d[2] + "-" + d[1] + "-" + d[0] + " " + time;
    }
}
