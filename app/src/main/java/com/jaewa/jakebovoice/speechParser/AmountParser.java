package com.jaewa.jakebovoice.speechParser;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cristian on 21/09/17.
 */

public class AmountParser extends SpeechParser<Float> {

    @Override
    public Map<ContentType, String> parse(String content) {

        try {
            String amount = content.replace(" ", "");
            amount = amount.replace("€", "");
            amount = amount.replace("Euro", "");
            amount = amount.replace("euro", "");
            amount = amount.replace(",", ".");
            amount = amount.replace(":", ".");

            Map<ContentType, String> result = new HashMap<>();
            result.put(ContentType.AMOUNT, formatAmount(Float.parseFloat(amount)) );

            return result;
        }catch(Exception e) {
            throw new IllegalArgumentException( "Unrecognized amount: " + content );
        }
    }

    @Override
    public Float convert(String content){
        try {
            String amount = parse(content).get(ContentType.AMOUNT);
            return Float.parseFloat(amount);
        }catch(Exception e) {
            throw new IllegalArgumentException( "Unrecognized amount: " + content );
        }
    }

    public static String formatAmount(Float amount){
        return String.format("%.02f", amount).replace(",", ".");
    }
}
