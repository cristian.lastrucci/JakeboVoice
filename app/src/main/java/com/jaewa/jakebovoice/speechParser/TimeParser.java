package com.jaewa.jakebovoice.speechParser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by cristian on 28/09/17.
 */

public class TimeParser extends SpeechParser<Date> {

    static DateFormat timeFormat;

    static {
        timeFormat = new SimpleDateFormat("HH:mm", Locale.ITALIAN);
    }

    @Override
    public Map<ContentType, String> parse(String content) {
        try {
            String[] time = content.split(":");
            Integer hour = Integer.parseInt(time[0]);
            Integer minute = Integer.parseInt(time[1]);
            String hourText = hour<10 ? "0"+String.valueOf(hour) : String.valueOf(hour);
            String minuteText = minute<10 ? "0"+String.valueOf(minute) : String.valueOf(minute);

            Map<ContentType, String> result = new HashMap<>();
            result.put(ContentType.TIME, (hourText + ":" + minuteText));
            return result;
        }catch (Exception e){
            throw new IllegalArgumentException( "Unrecognized time: " + content );
        }
    }

    @Override
    public Date convert(String content){
        try {
            String time = parse(content).get(ContentType.TIME);
            return timeFormat.parse(time);
        }catch(Exception e) {
            throw new IllegalArgumentException( "Unrecognized date: " + content );
        }
    }
}
