package com.jaewa.jakebovoice.speechParser;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cristian on 05/10/17.
 */

public class OperationParser extends SpeechParser<ParserType> {

    private ParserType operationType;

    public OperationParser(ParserType parserType){

        if(parserType != null && (ParserType.INSERTION.equals(parserType) || ParserType.SEARCH.equals(parserType)) ){
            operationType = parserType;
        }
    }

    @Override
    public Map<ContentType, String> parse(String content) {

        if( operationType == null ){
            throw new IllegalArgumentException( "Unrecognized operation." );
        }

        Map<ContentType, String> result = new HashMap<>();
        result.put(ContentType.OPERATION, operationType.toString());
        return result;
    }

    @Override
    public ParserType convert(String content){
        try {
            String operationType = parse(content).get(ContentType.OPERATION);
            return ParserType.valueOf(operationType);
        }catch(Exception e) {
            throw new IllegalArgumentException( "Unrecognized operation: " + content );
        }
    }
}
