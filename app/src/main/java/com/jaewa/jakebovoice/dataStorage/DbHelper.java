package com.jaewa.jakebovoice.dataStorage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by cristian on 30/09/17.
 */

public class DbHelper extends SQLiteOpenHelper {

    public DbHelper(Context context){
        super(context, DbUtils.DATABASE_NAME, null, DbUtils.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL( "CREATE TABLE "+ DbUtils.TABLE_NAME + " ( " +
                            DbUtils.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            DbUtils.FIELD_DATETIME + " DATETIME NOT NULL, " +
                            DbUtils.FIELD_AMOUNT + " TEXT NOT NULL, " +
                            DbUtils.FIELD_CATEGORY + " TEXT NOT NULL, " +
                            DbUtils.FIELD_DESCRIPTION + " TEXT )" );
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {

    }
}
