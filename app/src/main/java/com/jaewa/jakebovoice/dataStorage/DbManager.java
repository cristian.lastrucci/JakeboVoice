package com.jaewa.jakebovoice.dataStorage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import static com.jaewa.jakebovoice.dataStorage.DbUtils.TABLE_NAME;

/**
 * Created by cristian on 30/09/17.
 */

public class DbManager {

    private Context context;
    private SQLiteDatabase db;
    private DbHelper dbHelper;

    public DbManager(Context context){
        this.context = context;
    }

    public void openConnection() throws SQLException {
        dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    public void closeConnection(){
        dbHelper.close();
    }

    private ContentValues createContentValues(String datetime, String amount, String category, String description ){
        ContentValues values = new ContentValues();
        values.put(DbUtils.FIELD_DATETIME, datetime);
        values.put(DbUtils.FIELD_AMOUNT, amount);
        values.put(DbUtils.FIELD_CATEGORY, category);
        values.put(DbUtils.FIELD_DESCRIPTION, description);
        return values;
    }

    public long insertMovement(String datetime, String amount, String category, String description){
        ContentValues values = createContentValues(datetime, amount, category, description);
        return db.insertOrThrow(TABLE_NAME, null, values);
    }

    public boolean updateMovement(long id, String datetime, String amount, String category, String description){
        ContentValues values = createContentValues(datetime, amount, category, description);
        return db.update(TABLE_NAME, values, DbUtils.FIELD_ID + "=" + id, null) > 0;
    }

    public boolean deleteMovement(long id){
        return db.delete(TABLE_NAME, DbUtils.FIELD_ID + "=" + id, null) > 0;
    }

    public Cursor findAllMovements(){
        return db.query(TABLE_NAME,
                        new String[]{DbUtils.FIELD_ID, DbUtils.FIELD_DATETIME, DbUtils.FIELD_AMOUNT, DbUtils.FIELD_CATEGORY, DbUtils.FIELD_DESCRIPTION},
                        null,
                        null,
                        null,
                        null,
                        DbUtils.FIELD_DATETIME + " DESC");
    }

    public Cursor findMovementById(long id){
        return db.query(TABLE_NAME,
                new String[]{DbUtils.FIELD_ID, DbUtils.FIELD_DATETIME, DbUtils.FIELD_AMOUNT, DbUtils.FIELD_CATEGORY, DbUtils.FIELD_DESCRIPTION},
                DbUtils.FIELD_ID + "=" + id,
                null,
                null,
                null,
                null,
                DbUtils.FIELD_DATETIME + " DESC");
    }

    public Cursor findMovementsByCategory(String category){

        String a = "";

        return db.query(TABLE_NAME,
                new String[]{DbUtils.FIELD_ID, DbUtils.FIELD_DATETIME, DbUtils.FIELD_AMOUNT, DbUtils.FIELD_CATEGORY, DbUtils.FIELD_DESCRIPTION},
                DbUtils.FIELD_CATEGORY + "=?",
                new String[]{category},
                null,
                null,
                DbUtils.FIELD_DATETIME + " DESC");
    }

    public Cursor findMovementsByPeriod(String fromDate, String toDate){

        String from = fromDate.replace("/", "-") + " 00:00:00";
        String to = toDate.replace("/", "-") + " 23:59:59";

        return db.query(TABLE_NAME,
                new String[]{DbUtils.FIELD_ID, DbUtils.FIELD_DATETIME, DbUtils.FIELD_AMOUNT, DbUtils.FIELD_CATEGORY, DbUtils.FIELD_DESCRIPTION},
                DbUtils.FIELD_DATETIME + " >= ? AND " + DbUtils.FIELD_DATETIME + " <= ?",
                new String[]{ from, to},
                null,
                null,
                DbUtils.FIELD_DATETIME + " DESC");
    }

    public Cursor findMovementsByPeriodAndCategory(String fromDate, String toDate, String category) {

        String from = fromDate.replace("/", "-") + " 00:00";
        String to = toDate.replace("/", "-") + " 23:59";

        return db.query(TABLE_NAME,
                new String[]{DbUtils.FIELD_ID, DbUtils.FIELD_DATETIME, DbUtils.FIELD_AMOUNT, DbUtils.FIELD_CATEGORY, DbUtils.FIELD_DESCRIPTION},
                DbUtils.FIELD_DATETIME + " >= ? AND " + DbUtils.FIELD_DATETIME + " <= ? AND " + DbUtils.FIELD_CATEGORY + " = ?",
                new String[]{from, to, category},
                null,
                null,
                DbUtils.FIELD_DATETIME + " DESC");
    }

    public long getMovementsCount() {
        return DatabaseUtils.queryNumEntries(db, TABLE_NAME);
    }

    public Cursor findIncome(String fromDate, String toDate) {

        return findMovementsByPeriodAndCategory(fromDate, toDate, "Entrata");
    }

    public Cursor findOutflow(String fromDate, String toDate) {

        String from = fromDate.replace("/", "-") + " 00:00";
        String to = toDate.replace("/", "-") + " 23:59";

        return db.query(TABLE_NAME,
                new String[]{DbUtils.FIELD_ID, DbUtils.FIELD_DATETIME, DbUtils.FIELD_AMOUNT, DbUtils.FIELD_CATEGORY, DbUtils.FIELD_DESCRIPTION},
                DbUtils.FIELD_DATETIME + " >= ? AND " + DbUtils.FIELD_DATETIME + " <= ? AND " + DbUtils.FIELD_CATEGORY + " != ?",
                new String[]{from, to, "Entrata"},
                null,
                null,
                DbUtils.FIELD_DATETIME + " DESC");
    }
}
