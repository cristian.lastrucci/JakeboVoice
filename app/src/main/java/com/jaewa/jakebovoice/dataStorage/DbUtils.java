package com.jaewa.jakebovoice.dataStorage;

/**
 * Created by cristian on 30/09/17.
 */

public class DbUtils {

    static final String DATABASE_NAME = "jakeboDB";
    static final int DATABASE_VERSION = 1;

    public static final String TABLE_NAME = "movements";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_DATETIME = "datetime";
    public static final String FIELD_AMOUNT = "amount";
    public static final String FIELD_CATEGORY = "category";
    public static final String FIELD_DESCRIPTION = "description";

}
