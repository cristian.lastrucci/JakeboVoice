package com.jaewa.jakebovoice.dataStorage;

/**
 * Created by cristian on 06/10/17.
 */

public class DbInitializer {

    public static void initDb( DbManager dbManager){

        try{

            dbManager.openConnection();

            dbManager.insertMovement("2016-12-02 09:45", "1760.30", "Entrata", "stipendio dicembre");
            dbManager.insertMovement("2016-12-02 11:30", "2.90", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2016-12-02 18:40", "3.20", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2016-12-11 16:27", "126.80", "Sussistenza", "bolletta luce e gas");
            dbManager.insertMovement("2016-12-28 09:15", "3.10", "Sussistenza", "pane e latte");

            dbManager.insertMovement("2017-04-03 11:20", "1307.00", "Entrata", "stipendio aprile");
            dbManager.insertMovement("2017-04-12 19:48", "128.40", "Sussistenza", "bolletta luce e gas");
            dbManager.insertMovement("2017-04-27 09:15", "2.80", "Sussistenza", "pane e latte");

            dbManager.insertMovement("2017-05-07 20:34", "1285.00", "Entrata", "stipendio maggio");
            dbManager.insertMovement("2017-05-08 19:35", "2.80", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-05-11 18:30", "117.00", "Sussistenza", "bolletta luce e gas");
            dbManager.insertMovement("2017-05-29 13:20", "2.80", "Sussistenza", "pane e latte");

            dbManager.insertMovement("2017-06-01 12:20", "2.95", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-06-05 20:34", "1245.60", "Entrata", "stipendio giugno");
            dbManager.insertMovement("2017-06-08 13:27", "3.00", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-06-15 09:21", "131.03", "Sussistenza", "bolletta luce e gas");
            dbManager.insertMovement("2017-06-22 14:02", "3.05", "Sussistenza", "pane e latte");

            dbManager.insertMovement("2017-07-08 17:03", "1240.55", "Entrata", "stipendio luglio");
            dbManager.insertMovement("2017-07-13 11:21", "104.30", "Sussistenza", "bolletta luce e gas");
            dbManager.insertMovement("2017-07-14 15:56", "2.55", "Sussistenza", "pane e latte");

            dbManager.insertMovement("2017-08-03 11:30", "1409.00", "Entrata", "stipendio agosto");
            dbManager.insertMovement("2017-08-16 12:15", "97.00", "Sussistenza", "bolletta luce e gas");

            dbManager.insertMovement("2017-09-04 12:27", "1389.00", "Entrata", "stipendio settembre");
            dbManager.insertMovement("2017-09-04 18:40", "89.25", "Sussistenza", "spesa esselunga");
            dbManager.insertMovement("2017-09-07 19:00", "55.00", "Hobby", "abbonamento mensile palestra");
            dbManager.insertMovement("2017-09-14 12:15", "123.45", "Sussistenza", "bolletta luce e gas");
            dbManager.insertMovement("2017-09-29 21:30", "7.50", "Hobby", "cinema");

            dbManager.insertMovement("2017-10-01 13:40", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-10-01 18:15", "134.25", "Sussistenza", "spesa coop");
            dbManager.insertMovement("2017-10-02 09:30", "1450.00", "Entrata", "stipendio ottobre");
            dbManager.insertMovement("2017-10-02 20:45", "25.00", "Hobby", "cena al messicano");
            dbManager.insertMovement("2017-10-03 11:54", "3.20", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-10-03 13:50", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-10-04 20:45", "25.00", "Inaspettata", "ruota bucata");
            dbManager.insertMovement("2017-10-06 16:04", "2.50", "Optional", "gelato");
            dbManager.insertMovement("2017-10-07 13:47", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-10-07 18:55", "55.00", "Hobby", "abbonamento mensile palestra");
            dbManager.insertMovement("2017-10-08 12:03", "3.30", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-10-10 13:58", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-10-12 21:00", "15.00", "Optional", "pizzeria");
            dbManager.insertMovement("2017-10-15 21:30", "8.00", "Hobby", "cinema uci");
            dbManager.insertMovement("2017-10-18 13:48", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-10-21 14:45", "3.15", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-10-23 13:21", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-10-24 21:00", "7.50", "Hobby", "cinema fulgor");
            dbManager.insertMovement("2017-10-27 16:31", "3.08", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-10-29 13:44", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-10-29 20:45", "15.00", "Optional", "pizzeria");
            dbManager.insertMovement("2017-10-30 12:15", "120.50", "Sussistenza", "bolletta luce e gas");
            dbManager.insertMovement("2017-10-30 12:24", "3.35", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-10-31 13:30", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-10-31 20:45", "30.00", "Hobby", "ingresso terme");

            dbManager.insertMovement("2017-11-02 13:52", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-11-03 13:30", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-11-03 13:57", "3.10", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-11-03 16:45", "240.00", "Inaspettata", "riparazione lavandino bagno");
            dbManager.insertMovement("2017-11-04 13:48", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-11-05 12:27", "1503.20", "Entrata", "stipendio novembre");
            dbManager.insertMovement("2017-11-06 13:58", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-11-07 20:45", "55.00", "Hobby", "abbonamento mensile palestra");
            dbManager.insertMovement("2017-11-08 13:50", "3.20", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-11-09 13:29", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-11-10 18:15", "16.00", "Sussistenza", "benzina");
            dbManager.insertMovement("2017-11-11 19:00", "24.00", "Optional", "felpa");
            dbManager.insertMovement("2017-11-11 20:30", "15.00", "Hobby", "pizzeria");
            dbManager.insertMovement("2017-11-12 12:15", "120.50", "Sussistenza", "bolletta luce e gas");
            dbManager.insertMovement("2017-11-20 13:51", "3.15", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-11-24 13:32", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-11-28 13:43", "1.00", "Optional", "caffè");

            dbManager.insertMovement("2017-12-02 13:48", "3.40", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-12-04 18:20", "1840.00", "Entrata", "stipendio dicembre");
            dbManager.insertMovement("2017-12-07 13:36", "3.20", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-12-07 19:31", "55.00", "Hobby", "abbonamento mensile palestra");
            dbManager.insertMovement("2017-12-09 21:00", "15.00", "Optional", "pizzeria");
            dbManager.insertMovement("2017-12-13 17:17", "144.00", "Sussistenza", "bolletta luce e gas");
            dbManager.insertMovement("2017-12-15 13:52", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-12-19 13:45", "3.05", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-12-19 13:49", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-12-20 13:50", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-12-21 13:50", "1.00", "Optional", "caffè");
            dbManager.insertMovement("2017-12-23 14:20", "2.80", "Sussistenza", "pane e latte");
            dbManager.insertMovement("2017-12-28 14:45", "45.00", "Inaspettata", "sostituzione catene neve");

            dbManager.insertMovement("2018-01-07 19:31", "55.00", "Hobby", "abbonamento mensile palestra");
            dbManager.insertMovement("2018-01-10 11:44", "1357.20", "Entrata", "stipendio gennaio");
            dbManager.insertMovement("2018-01-15 17:17", "118.90", "Sussistenza", "bolletta luce e gas");

            dbManager.insertMovement("2018-02-03 14:01", "1206.04", "Entrata", "stipendio febbraio");
            dbManager.insertMovement("2018-02-07 19:31", "55.00", "Hobby", "abbonamento mensile palestra");
            dbManager.insertMovement("2018-02-12 16:20", "120.00", "Sussistenza", "bolletta luce e gas");

            dbManager.insertMovement("2018-03-11 16:58", "1487.23", "Entrata", "stipendio marzo");
            dbManager.insertMovement("2018-03-13 15:30", "137.50", "Sussistenza", "bolletta luce e gas");

            dbManager.insertMovement("2018-04-04 12:50", "1501.00", "Entrata", "stipendio aprile");
            dbManager.insertMovement("2018-04-12 18:10", "125.70", "Sussistenza", "bolletta luce e gas");

            dbManager.closeConnection();

        }catch (Exception e ){
            System.out.println("Could not init database: " + e);
        }
    }
}
