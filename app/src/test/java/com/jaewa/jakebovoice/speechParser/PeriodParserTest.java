package com.jaewa.jakebovoice.speechParser;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by cristian on 17/10/17.
 */

public class PeriodParserTest {

    private SpeechParser parser;

    @Before
    public void setUp() {
        parser = new PeriodParser();
    }

    @Test
    public void testPeriodParsing1(){
        Map<ContentType, String> result = parser.parse("1947");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals("01/01/1947", result.get(ContentType.DATE_FROM));
        assertEquals("31/12/1947", result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing2(){
        Map<ContentType, String> result = parser.parse("2134");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals("01/01/2134", result.get(ContentType.DATE_FROM));
        assertEquals("31/12/2134", result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing3(){
        Calendar today = Calendar.getInstance();
        Map<ContentType, String> result = parser.parse("agosto");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals("01/08/" + today.get(Calendar.YEAR), result.get(ContentType.DATE_FROM));
        assertEquals("31/08/" + today.get(Calendar.YEAR), result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing4(){

        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_YEAR, -1);

        Map<ContentType, String> result = parser.parse("Ieri");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing5(){

        Calendar date = Calendar.getInstance();

        Map<ContentType, String> result = parser.parse("oggi");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing6(){

        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_YEAR, 1);

        Map<ContentType, String> result = parser.parse("Domani");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing7(){

        Calendar from = Calendar.getInstance();
        from.add(Calendar.DAY_OF_YEAR, DateUtils.dayOfWeek("lunedì") - DateUtils.dayOfWeek(from) - 7);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.DAY_OF_YEAR, DateUtils.dayOfWeek("lunedì") - DateUtils.dayOfWeek(to) - 1);

        Map<ContentType, String> result = parser.parse("settimana Scorsa");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals(DateUtils.formatDate(from), result.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(to), result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing8(){

        Calendar from = Calendar.getInstance();
        from.set(Calendar.DAY_OF_MONTH, from.getActualMaximum(Calendar.DAY_OF_MONTH));
        from.add(Calendar.DAY_OF_YEAR, 1);

        Calendar to = Calendar.getInstance();
        to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));
        to.add(Calendar.DAY_OF_YEAR, 1);
        to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));

        Map<ContentType, String> result = parser.parse("prossimo mese");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals(DateUtils.formatDate(from), result.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(to), result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing9(){

        Calendar from = Calendar.getInstance();
        from.set(Calendar.DAY_OF_MONTH, 1);

        Calendar to = Calendar.getInstance();
        to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));

        Map<ContentType, String> result = parser.parse("questo mese");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals(DateUtils.formatDate(from), result.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(to), result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing10(){

        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, -1);
        from.set(Calendar.DAY_OF_MONTH, 1);
        from.set(Calendar.MONTH, 0);

        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, -1);
        to.set(Calendar.DAY_OF_MONTH, 31);
        to.set(Calendar.MONTH, 11);

        Map<ContentType, String> result = parser.parse("Anno Scorso");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals(DateUtils.formatDate(from), result.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(to), result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing11(){

        Calendar from = Calendar.getInstance();
        from.set(Calendar.YEAR, 2004);
        from.set(Calendar.MONTH, 2);
        from.set(Calendar.DAY_OF_MONTH, 1);

        Calendar to = Calendar.getInstance();
        to.set(Calendar.YEAR, 2004);
        to.set(Calendar.MONTH, 2);
        to.set(Calendar.DAY_OF_MONTH, 31);

        Map<ContentType, String> result = parser.parse("marzo 2004");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals(DateUtils.formatDate(from), result.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(to), result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing12(){

        Calendar date = Calendar.getInstance();
        date.set(Calendar.YEAR, 2017);
        date.set(Calendar.MONTH, 9);
        date.set(Calendar.DAY_OF_MONTH, 17);

        Map<ContentType, String> result = parser.parse("17 ottobre 2017");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing13(){

        Calendar from = Calendar.getInstance();
        from.set(Calendar.YEAR, 2017);
        from.set(Calendar.MONTH, 9);
        from.set(Calendar.DAY_OF_MONTH, 17);

        Calendar to = Calendar.getInstance();
        to.set(Calendar.YEAR, 2019);
        to.set(Calendar.MONTH, 7);
        to.set(Calendar.DAY_OF_MONTH, 24);

        Map<ContentType, String> result = parser.parse("Da 17 ottobre 2017 a 24 agosto 2019");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals(DateUtils.formatDate(from), result.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(to), result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing14(){

        Calendar from = Calendar.getInstance();
        from.set(Calendar.YEAR, 2017);
        from.set(Calendar.MONTH, 0);
        from.set(Calendar.DAY_OF_MONTH, 12);

        Calendar to = Calendar.getInstance();
        to.set(Calendar.YEAR, 2017);
        to.set(Calendar.MONTH, 0);
        to.set(Calendar.DAY_OF_MONTH, 16);

        Map<ContentType, String> result = parser.parse("Dal 12 gennaio 2017 al 16 gennaio 2017");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals(DateUtils.formatDate(from), result.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(to), result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing15(){

        Calendar from = Calendar.getInstance();
        from.set(Calendar.YEAR, 2016);
        from.set(Calendar.MONTH, 0);
        from.set(Calendar.DAY_OF_MONTH, 16);

        Calendar to = Calendar.getInstance();
        to.set(Calendar.YEAR, 2017);
        to.set(Calendar.MONTH, 2);
        to.set(Calendar.DAY_OF_MONTH, 20);

        Map<ContentType, String> result = parser.parse("Dal 20 marzo 2017 al 16 gennaio 2016");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals(DateUtils.formatDate(from), result.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(to), result.get(ContentType.DATE_TO));
    }

    @Test
    public void testPeriodParsing16(){

        Map<ContentType, String> result = parser.parse("Sempre");
        assertEquals(2, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE_FROM));
        assertTrue(result.containsKey(ContentType.DATE_TO));
        assertEquals("sempre", result.get(ContentType.DATE_FROM));
        assertEquals("sempre", result.get(ContentType.DATE_TO));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPeriodParsing17(){
        Map<ContentType, String> result = parser.parse("5 marzo 2017 8 marzo 2017");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPeriodParsing18(){
        Map<ContentType, String> result = parser.parse("5 marzo 2017 - 8 marzo 2017");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPeriodParsing19(){
        Map<ContentType, String> result = parser.parse("da 05/03/2017 a 14/03/2017");
    }
}
