package com.jaewa.jakebovoice.speechParser;

import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Created by cristian on 17/10/17.
 */

public class SpeechUtilsTest {

    @Test
    public void isKeywordTest(){

        assertTrue(SpeechUtils.isKeyword("inserisci"));
        assertTrue(SpeechUtils.isKeyword("registra"));
        assertTrue(SpeechUtils.isKeyword("trova"));
        assertTrue(SpeechUtils.isKeyword("data"));
        assertTrue(SpeechUtils.isKeyword("importo"));

        assertFalse(SpeechUtils.isKeyword("pippo"));
        assertFalse(SpeechUtils.isKeyword("orario"));
        assertFalse(SpeechUtils.isKeyword("anno"));
    }

    @Test
    public void getParserTest(){

        assertTrue( SpeechUtils.getParser("inserisci") instanceof OperationParser );
        assertTrue( SpeechUtils.getParser("cerca") instanceof OperationParser );
        assertTrue( SpeechUtils.getParser("periodo") instanceof PeriodParser );
        assertTrue( SpeechUtils.getParser("categoria") instanceof CategoryParser );
        assertTrue( SpeechUtils.getParser("ore") instanceof TimeParser );

        assertNull(SpeechUtils.getParser("pippo"));
        assertNull(SpeechUtils.getParser("orario"));
        assertNull(SpeechUtils.getParser("anno"));
    }
}
