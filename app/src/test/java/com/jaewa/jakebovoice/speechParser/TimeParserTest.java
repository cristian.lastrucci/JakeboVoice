package com.jaewa.jakebovoice.speechParser;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by cristian on 17/10/17.
 */

public class TimeParserTest {

    private SpeechParser parser;

    @Before
    public void setUp() {
        parser = new TimeParser();
    }

    @Test
    public void testTimeParsing1(){
        Map<ContentType, String> result = parser.parse("5:7");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.TIME));
        assertEquals("05:07", result.get(ContentType.TIME));
    }

    @Test
    public void testTimeParsing2(){
        Map<ContentType, String> result = parser.parse("05:07");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.TIME));
        assertEquals("05:07", result.get(ContentType.TIME));
    }

    @Test
    public void testTimeParsing3(){
        Map<ContentType, String> result = parser.parse("17:8");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.TIME));
        assertEquals("17:08", result.get(ContentType.TIME));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTimeParsing4(){
        Map<ContentType, String> result = parser.parse("05:20 pm");
    }
}
