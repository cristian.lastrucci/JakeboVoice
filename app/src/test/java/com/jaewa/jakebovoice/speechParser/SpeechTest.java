package com.jaewa.jakebovoice.speechParser;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;

/**
 * Created by cristian on 17/10/17.
 */

public class SpeechTest {

    private Speech speech;

    @Before
    public void setUp() {
        speech = new Speech();
    }

    @Test
    public void testSpeechParsing1(){
        speech.parse("Inserisci importo 7,50 € categoria hobby descrizione biglietto cinema");
        assertEquals("7.50", speech.get(ContentType.AMOUNT));
        assertEquals("Hobby", speech.get(ContentType.CATEGORY));
        assertEquals("biglietto cinema", speech.get(ContentType.DESCRIPTION));
        assertEquals("", speech.get(ContentType.DATE));
        assertEquals("", speech.get(ContentType.TIME));
        assertEquals("", speech.get(ContentType.DATE_FROM));
        assertEquals("", speech.get(ContentType.DATE_TO));
    }

    @Test
    public void testSpeechParsing2(){
        speech.parse("Inserisci descrizione biglietto cinema categoria hobby importo 7,50 €");
        assertEquals("7.50", speech.get(ContentType.AMOUNT));
        assertEquals("Hobby", speech.get(ContentType.CATEGORY));
        assertEquals("biglietto cinema", speech.get(ContentType.DESCRIPTION));
        assertEquals("", speech.get(ContentType.DATE));
        assertEquals("", speech.get(ContentType.TIME));
        assertEquals("", speech.get(ContentType.DATE_FROM));
        assertEquals("", speech.get(ContentType.DATE_TO));
    }

    @Test
    public void testSpeechParsing3(){
        Calendar today = Calendar.getInstance();
        speech.parse("aggiungi data oggi ora 5:45 categoria sussistenza");
        assertEquals(DateUtils.formatDate(today), speech.get(ContentType.DATE));
        assertEquals("05:45", speech.get(ContentType.TIME));
        assertEquals("Sussistenza", speech.get(ContentType.CATEGORY));
        assertEquals("", speech.get(ContentType.AMOUNT));
        assertEquals("", speech.get(ContentType.DESCRIPTION));
        assertEquals("", speech.get(ContentType.DATE_FROM));
        assertEquals("", speech.get(ContentType.DATE_TO));
    }

    @Test
    public void testSpeechParsing4(){
        Calendar today = Calendar.getInstance();
        speech.parse("cerca periodo oggi categoria sussistenza");
        assertEquals(DateUtils.formatDate(today), speech.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(today), speech.get(ContentType.DATE_TO));
        assertEquals("Sussistenza", speech.get(ContentType.CATEGORY));
        assertEquals("", speech.get(ContentType.DATE));
        assertEquals("", speech.get(ContentType.TIME));
        assertEquals("", speech.get(ContentType.AMOUNT));
        assertEquals("", speech.get(ContentType.DESCRIPTION));
    }

    @Test
    public void testSpeechParsing5(){
        Calendar from = Calendar.getInstance();
        from.set(Calendar.DAY_OF_MONTH, 1);

        Calendar to = Calendar.getInstance();
        to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));

        speech.parse("filtra periodo mese corrente");
        assertEquals(DateUtils.formatDate(from), speech.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(to), speech.get(ContentType.DATE_TO));
        assertEquals("", speech.get(ContentType.CATEGORY));
        assertEquals("", speech.get(ContentType.DATE));
        assertEquals("", speech.get(ContentType.TIME));
        assertEquals("", speech.get(ContentType.AMOUNT));
        assertEquals("", speech.get(ContentType.DESCRIPTION));
    }

    @Test
    public void testSpeechParsing6(){
        speech.parse("trova categoria entrata");
        assertEquals("Entrata", speech.get(ContentType.CATEGORY));
        assertEquals("", speech.get(ContentType.DATE_FROM));
        assertEquals("", speech.get(ContentType.DATE_TO));
        assertEquals("", speech.get(ContentType.DATE));
        assertEquals("", speech.get(ContentType.TIME));
        assertEquals("", speech.get(ContentType.AMOUNT));
        assertEquals("", speech.get(ContentType.DESCRIPTION));
    }

    @Test
    public void testSpeechParsing7(){

        Calendar from = Calendar.getInstance();
        from.set(Calendar.YEAR, 2017);
        from.set(Calendar.MONTH, 7);
        from.set(Calendar.DAY_OF_MONTH, 15);

        Calendar to = Calendar.getInstance();
        to.set(Calendar.YEAR, 2017);
        to.set(Calendar.MONTH, 7);
        to.set(Calendar.DAY_OF_MONTH, 22);

        speech.parse("cerca categoria optional periodo da 15 agosto 2017 al 22 agosto 2017");
        assertEquals(DateUtils.formatDate(from), speech.get(ContentType.DATE_FROM));
        assertEquals(DateUtils.formatDate(to), speech.get(ContentType.DATE_TO));
        assertEquals("Optional", speech.get(ContentType.CATEGORY));
        assertEquals("", speech.get(ContentType.DATE));
        assertEquals("", speech.get(ContentType.TIME));
        assertEquals("", speech.get(ContentType.AMOUNT));
        assertEquals("", speech.get(ContentType.DESCRIPTION));
    }

}
