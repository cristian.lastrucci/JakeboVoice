package com.jaewa.jakebovoice.speechParser;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by cristian on 17/10/17.
 */

public class OperationParserTest {

    private SpeechParser parser;

    @Test
    public void testOperationParsing1(){
        parser = new OperationParser(ParserType.INSERTION);
        Map<ContentType, String> result = parser.parse("");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.OPERATION));
        assertEquals(ParserType.INSERTION.toString(), result.get(ContentType.OPERATION));
    }

    @Test
    public void testOperationParsing2(){
        parser = new OperationParser(ParserType.SEARCH);
        Map<ContentType, String> result = parser.parse("pippo");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.OPERATION));
        assertEquals(ParserType.SEARCH.toString(), result.get(ContentType.OPERATION));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOperationParsing3(){
        parser = new OperationParser(ParserType.AMOUNT);
        Map<ContentType, String> result = parser.parse("");
    }
}
