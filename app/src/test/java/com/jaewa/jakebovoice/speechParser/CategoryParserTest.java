package com.jaewa.jakebovoice.speechParser;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;

/**
 * Created by cristian on 17/10/17.
 */

public class CategoryParserTest {

    private SpeechParser parser;

    @Before
    public void setUp() {
        parser = new CategoryParser();
    }

    @Test
    public void testCategoryParsing1(){
        Map<ContentType, String> result = parser.parse("sussistenza");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.CATEGORY));
        assertEquals("Sussistenza", result.get(ContentType.CATEGORY));

    }

    @Test
    public void testCategoryParsing2(){
        Map<ContentType, String> result = parser.parse("Sussistenza");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.CATEGORY));
        assertEquals("Sussistenza", result.get(ContentType.CATEGORY));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCategoryParsing4(){
        Map<ContentType, String> result = parser.parse("pippo");
    }

}
