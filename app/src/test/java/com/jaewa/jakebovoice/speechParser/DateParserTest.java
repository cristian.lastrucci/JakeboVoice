package com.jaewa.jakebovoice.speechParser;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by cristian on 17/10/17.
 */

public class DateParserTest {

    private SpeechParser parser;

    @Before
    public void setUp() {
        parser = new DateParser();
    }

    @Test
    public void testDateParsing1(){
        Map<ContentType, String> result = parser.parse("17 ottobre 2017");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE));
        assertEquals("17/10/2017", result.get(ContentType.DATE));
    }

    @Test
    public void testDateParsing2(){
        Map<ContentType, String> result = parser.parse("17 Ottobre 2017");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE));
        assertEquals("17/10/2017", result.get(ContentType.DATE));
    }

    @Test
    public void testDateParsing3(){
        Map<ContentType, String> result = parser.parse("17 10 2017");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE));
        assertEquals("17/10/2017", result.get(ContentType.DATE));
    }

    @Test
    public void testDateParsing4(){
        Map<ContentType, String> result = parser.parse("5 9 2017");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE));
        assertEquals("05/09/2017", result.get(ContentType.DATE));
    }

    @Test
    public void testDateParsing5(){

        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_YEAR, DateUtils.dayOfWeek("venerdì") - DateUtils.dayOfWeek(date));

        Map<ContentType, String> result = parser.parse("venerdì");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE));
    }

    @Test
    public void testDateParsing6(){

        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_YEAR, DateUtils.dayOfWeek("domenica") - DateUtils.dayOfWeek(date));

        Map<ContentType, String> result = parser.parse("Domenica");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE));
    }

    @Test
    public void testDateParsing7(){

        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_YEAR, DateUtils.dayOfWeek("giovedì") - DateUtils.dayOfWeek(date) - 7);

        Map<ContentType, String> result = parser.parse("Giovedì Scorso");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE));
    }

    @Test
    public void testDateParsing8(){

        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_YEAR, DateUtils.dayOfWeek("giovedì") - DateUtils.dayOfWeek(date) - 7);

        Map<ContentType, String> result = parser.parse("scorso giovedì");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE));
    }

    @Test
    public void testDateParsing9(){

        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_YEAR, DateUtils.dayOfWeek("martedì") - DateUtils.dayOfWeek(date) + 7);

        Map<ContentType, String> result = parser.parse("martedì prossimo");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE));
    }

    @Test
    public void testDateParsing10(){

        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_YEAR, DateUtils.dayOfWeek("martedì") - DateUtils.dayOfWeek(date) + 7);

        Map<ContentType, String> result = parser.parse("prossimo martedì");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE));
    }

    @Test
    public void testDateParsing11(){

        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_YEAR, -1);

        Map<ContentType, String> result = parser.parse("Ieri");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE));
    }

    @Test
    public void testDateParsing12(){

        Calendar date = Calendar.getInstance();

        Map<ContentType, String> result = parser.parse("oggi");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE));
    }

    @Test
    public void testDateParsing13(){

        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_YEAR, 1);

        Map<ContentType, String> result = parser.parse("Domani");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DATE));
        assertEquals(DateUtils.formatDate(date), result.get(ContentType.DATE));
    }
}
