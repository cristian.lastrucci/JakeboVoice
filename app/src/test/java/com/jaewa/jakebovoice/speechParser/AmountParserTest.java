package com.jaewa.jakebovoice.speechParser;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by cristian on 17/10/17.
 */

public class AmountParserTest {

    private SpeechParser parser;

    @Before
    public void setUp() {
        parser = new AmountParser();
    }

    @Test
    public void testAmountParsing1(){
        //Codifica google pronunciando: "dieci euro e trentasette centesimi" o "dieci euro e trentasette"
        Map<ContentType, String> result = parser.parse("€10,37");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.AMOUNT));
        assertEquals("10.37", result.get(ContentType.AMOUNT));
    }

    @Test
    public void testAmountParsing2(){
        //Codifica google pronunciando: "dieci euro punto trentasette"
        Map<ContentType, String> result = parser.parse("€10. 37");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.AMOUNT));
        assertEquals("10.37", result.get(ContentType.AMOUNT));
    }

    @Test
    public void testAmountParsing3(){
        //Codifica google pronunciando: "dieci euro virgola trentasette"
        Map<ContentType, String> result = parser.parse("€10, 37");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.AMOUNT));
        assertEquals("10.37", result.get(ContentType.AMOUNT));
    }

    @Test
    public void testAmountParsing4(){
        //Codifica google pronunciando: "euro dieci e trentasette"
        Map<ContentType, String> result = parser.parse("Euro 10:37");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.AMOUNT));
        assertEquals("10.37", result.get(ContentType.AMOUNT));
    }

    @Test
    public void testAmountParsing5(){
        //Codifica google pronunciando: "euro dieci punto trentasette"
        Map<ContentType, String> result = parser.parse("Euro 10.37");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.AMOUNT));
        assertEquals("10.37", result.get(ContentType.AMOUNT));
    }

    @Test
    public void testAmountParsing6(){
        //Codifica google pronunciando: "euro dieci virgola trentasette"
        Map<ContentType, String> result = parser.parse("Euro 10,37");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.AMOUNT));
        assertEquals("10.37", result.get(ContentType.AMOUNT));
    }

    @Test
    public void testAmountParsing7(){
        //Codifica google pronunciando: "dieci e trentasette"
        Map<ContentType, String> result = parser.parse("10:37");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.AMOUNT));
        assertEquals("10.37", result.get(ContentType.AMOUNT));
    }

    @Test
    public void testAmountParsing8(){
        //Codifica google pronunciando: "dieci punto trentasette"
        Map<ContentType, String> result = parser.parse("10.37");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.AMOUNT));
        assertEquals("10.37", result.get(ContentType.AMOUNT));
    }

    @Test
    public void testAmountParsing9(){
        //Codifica google pronunciando: "dieci virgola trentasette"
        Map<ContentType, String> result = parser.parse("10,37");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.AMOUNT));
        assertEquals("10.37", result.get(ContentType.AMOUNT));
    }

    @Test
    public void testAmountParsing10(){
        //Codifica google pronunciando: "dieci punto trentasette euro"
        Map<ContentType, String> result = parser.parse("10. €37");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.AMOUNT));
        assertEquals("10.37", result.get(ContentType.AMOUNT));
    }

    @Test
    public void testAmountParsing11(){
        //Codifica google pronunciando: "dieci virgola trentasette euro"
        Map<ContentType, String> result = parser.parse("10, €37");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.AMOUNT));
        assertEquals("10.37", result.get(ContentType.AMOUNT));
    }

    @Test
    public void testAmountParsing12(){
        Map<ContentType, String> result = parser.parse("10,37 €");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.AMOUNT));
        assertEquals("10.37", result.get(ContentType.AMOUNT));
    }

    @Test
    public void testAmountParsing13(){
        Map<ContentType, String> result = parser.parse("€ 1044,56");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.AMOUNT));
        assertEquals("1044.56", result.get(ContentType.AMOUNT));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAmountParsing14(){
        Map<ContentType, String> result = parser.parse("1.044,56");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAmountParsing15(){
        Map<ContentType, String> result = parser.parse("trentadue euro");
    }
}
