package com.jaewa.jakebovoice.speechParser;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by cristian on 17/10/17.
 */

public class DescriptionParserTest {

    private SpeechParser parser;

    @Before
    public void setUp() {
        parser = new DescriptionParser();
    }

    @Test
    public void testDescriptionParsing1(){
        Map<ContentType, String> result = parser.parse("B3nzin4 PeR AuTO");
        assertEquals(1, result.keySet().size());
        assertTrue(result.containsKey(ContentType.DESCRIPTION));
        assertEquals("B3nzin4 PeR AuTO", result.get(ContentType.DESCRIPTION));

    }
}
