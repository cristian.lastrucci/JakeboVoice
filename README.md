Jakebo Voice è un prototipo della versione mobile di [Jakebo](http://www.jakebo.it), 
che traendo ispirazione dal metodo giapponese Kakebo per la gestione dei conti 
domestici, offre la possibilità ai propri utenti di registrare le spese sostenute
e monitorare lo stato delle proprie finanze.<br /><br />
La prerogativa principale dell’app, che ne ha indirizzato progettazione e sviluppo, 
è la volontà di rendere semplici, veloci e intuitive quelle operazioni che l’utente 
è chiamato a svolgere diverse volte al giorno, quali l’inserimento di nuove spese
o la loro consultazione; dal momento che il gradimento di un utente non è determinato
uniformemente dall’insieme di tutte le funzionalità di un’app, ma è influenzato
maggiormente dalle tre/quattro operazioni che questo utilizza con maggior 
frequenza, l’idea è quella di individuare un’interfaccia che renda la loro
esecuzione il più “naturale” possibile, ed è per questa ragione che è stato
deciso di sviluppare un set di comandi vocali che rendesse semplice e intuitivo 
il dialogo dell’utente con l’app.<br /><br />
Per visualizzare la presentazione del progetto clicca [qui](/docs/JV-Presentation.pdf), 
mentre per visualizzare la relazione completa clicca [qui](/docs/JV-FinalRelation.pdf).<br /><br />
Per scaricare e provare Jakebo Voice clicca [qui](/jakebo-voice.apk).
